class ExecutionAbstractionJob < ApplicationJob
  queue_as :default

  def perform(job_name=nil, location=nil, options=nil)
    if location == nil
      raise 'Error, Location can\'t be nil'
      return nil
    end
    case job_name
    when 'run_migration'
      ExecutionAbstractionHelper.run_migration(location)
    when 'run_compositional_role'
      ExecutionAbstractionHelper.run_compositional_role(location)
    when 'run_health_report'
      ExecutionAbstractionHelper.run_health_report(location)
    when 'run_backup'
      ExecutionAbstractionHelper.run_backup(location)
    when 'start_stop_restart'
      ExecutionAbstractionHelper.start_stop_restart_service(location, service=options['service'], action=options['action'])
    when 'run_ourcompose_role'
      ExecutionAbstractionHelper.run_ourcompose_role(location, service=options['service'])
    else
      raise 'Error, Undefined method selected'
      return nil
    end
    # Do something later
  end
end
