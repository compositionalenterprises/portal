class ExecutionAbstractionController < ApplicationController

    def get_service_logs
        # Host level
        # Call to commands_receivable to dump log files to disk
        # param [:args]
    end

    def get_env_configuration
        service = service_params["service"]
        ## TODO:
        ## Not hardcoded collection_version/ Pull from ENV
        collection_version = 'stable-4.0'
        uri = URI("https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/raw/#{collection_version}/roles/#{service}/defaults/main.yml")
        request = Net::HTTP::Get.new(uri)
        request.content_type = "application/yml"
        response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme) do |http|
            http.request(request)
        end
        response_yml = YAML.load(response.body)

        # This is blacklist of keys that will not be shown to the user
        to_be_deleted = [
            'ourcompose_portal_production_key', 
            'ourcompose_portal_production_yml_enc',
        ]
        to_be_deleted.each do |element_to_be_deleted|
            response_yml.delete(element_to_be_deleted)
        end

        ## TODO: Show Structured Data
        # IE Bind mountpoints
        response_yml.keys.each do |f|
            if f.end_with?("_bind_mountpoints")
                response_yml.delete(f)
            end
        end

        # Updates default keys that already have a variable from the environment
        environment_variables = YAML.load_file('storage/ansible/environment/group_vars/compositional/all.yml')
        environment_variables.each do |key,value|
            if response_yml[key] != nil
                response_yml[key] = value
                puts response_yml[key]
            end
        end

        render :json => response_yml.to_json
    end

    def post_env_configuration
        service = params["service"]
        submitted_vars = params["yml_vars"]
        all_vars = ExecutionAbstractionHelper.current_env_vars(service)
        current_env_vars = YAML.load_file('storage/ansible/environment/group_vars/compositional/all.yml')
        
        submitted_vars.keys.each do |env_var_key|
            if submitted_vars[env_var_key] != all_vars[env_var_key]
                puts submitted_vars[env_var_key]
                current_env_vars[env_var_key] = submitted_vars[env_var_key]
            end
        end

        storage_root = "#{Rails.application.config.storage.root}"
        file_path = "#{storage_root}/ansible/environment/group_vars/compositional/all.yml"
        File.open("#{file_path}", 'w+') do |f|
            f.write(current_env_vars.to_yaml)
        end

        # Unsure if works or not...
        update_succeeded = true
        unless ENV['OURCOMPOSEBOT_RO_KEY'].blank?
            # Need to update the post_update_environment_repo creds key in credentials!
            post_response = Rundeck.post_update_environment_repo
            get_response = Rundeck.get_update_environment_repo(post_response)
            unless get_response['executionState'] == 'SUCCEEDED'
                update_succeeded = false
            end
        end
        results = {}
        results['env'] = YAML.load_file('storage/ansible/environment/group_vars/compositional/all.yml')
        results['update_succeeded'] = update_succeeded
        render :json => results.to_json

    end

    def get_service_state
        # Parse all the variables on the frontend for the users:
        # - ourcompose_common_services
        render :json => YAML.load_file('storage/ansible/environment/group_vars/compositional/all.yml').to_json
    end

    def post_service_state
        service = service_params["service"]
        action = service_action_params["service_action"]
        ExecutionAbstractionJob.perform_later('start_stop_restart', location='rundeck', options={'service'=> service, 'action'=> action} )
        # Run Action on Service via Rundeck
        render :json => {"service": service_params["service"], "action": service_action_params["service_action"] }
    end
 
    def get_service_user
        # TODO
        # Ability to check at host level what users are on service
    end

    def post_service_user
        # param [:username, :password, :state(present/absent)]
    end

    def post_migrate
        ExecutionAbstractionJob.perform_later('run_migration', job_location_params)
        render json: {tellUser: 'running_migration'}.to_json
    end

    def post_compositional_role
        puts service_params
        ExecutionAbstractionJob.perform_later('run_compositional_role', job_location_params)
        render json: {tellUser: 'running_comp_role'}.to_json
    end

    def post_run_ourcompose_role
        service = service_params['service']
        ExecutionAbstractionJob.perform_later('run_ourcompose_role', location='rundeck', options={'service'=> service })
        render json: {response: 'running_comp_role'}.to_json
    end

    def post_health_check
        ExecutionAbstractionJob.perform_later('run_health_check', 'local')
        render json: {tellUser:'running_health_check'}.to_json
    end

    def post_run_backup
        ExecutionAbstractionJob.perform_later('run_backup', job_location_params)
        render json: {tellUser:'running_health_check'}.to_json
    end

    def get_migration
        render json: {migrations: RundeckJob.where(job_name:"migration")}
    end

    def get_compositional_role
        render json: {compositional_roles: RundeckJob.where(job_name:"run_compositional_role")}
    end

    def get_health_check
        render json: {health_checks: HealthCheck.last, service_status: parse_last_status(HealthCheck.last.service_status)}
    end

    def get_backup
        render json: {backups: RundeckJob.where(job_name:"backup")}
    end

    private

    def parse_last_status(service_statuses)
        begin
            JSON.parse(service_statuses.gsub("=>", ":"))
        rescue JSON::ParserError
            return nil
        end
    end

    def job_location_params
        location = params.permit(:location)
        if location == "rundeck"
            return "rundeck"
        elsif location == "local"
            return "local"
        else
            return nil
        end
    end

    def service_params
        service = params.permit(:service)
    end

    def env_params
        params.permit([:service, :yml_vars])
    end

    def service_action_params
        state = params.permit(:service_action)
    end

end
