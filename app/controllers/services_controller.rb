class ServicesController < ApplicationController
    before_action :authenticate_admin!

    def show
        render json: {
            services: Service.all
        }
    end

    def create
        @new_service = Service.create!(name: service_params[:name], url: service_params[:url])
        render json: {
            service:  @new_service
        }
    end

    def update
    end

    def destroy
        @service = Service.find(service_params[:id])
        @service.destroy!
        render json: {
          service: @service
        }
    end

    private

    def service_params
        params.require(:service).permit(:id, :name, :url)
    end

end
