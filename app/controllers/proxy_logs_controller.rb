class ProxyLogsController < ApplicationController
    before_action :authenticate_admin!

    def show
       
    end

    def available_log_files
        render :json => Dir.entries(Rails.configuration.srv_logs).to_json
    end

    def logfile_length
        if !params[:backward].blank?
            count = %x{wc -l #{Rails.configuration.srv_logs + params[:backward]}}.split.first.to_i
            render json: {length: count}
        else
            render json: {length: nil}
        end
    end

    def log_file
        file_to_read = "portal_main_access.log"
        log_index = 0
        if !params[:location].blank?
            log_index = params[:location].to_i
        end
        a = []
        last_one = nil
        if params[:backward].blank?
            File::Tail::Logfile.open(Rails.configuration.srv_logs + file_to_read, :return_if_eof => true) do |log|
                begin
                    while(a.length < 200)
                        if a.length == 0
                            log.tail(1) do |line|
                                a.append(line)
                                log_index = log_index + 1
                            end
                        else
                            log.backward(log_index).tail(1) do |line|
                                if last_one == line
                                    a.append(nil)
                                else
                                    a.append(line)
                                    last_one = line
                                end
                                log_index = log_index + 1
                            end
                        end
                    end
                rescue File::Tail::BreakException
                    render :json => ["Made it to end of file!"]
                end
            end
        else
            if params[:backward] == "." || params[:backward] == ".."
                render :json => ["Invalid Request"]
                return
            end
            if params[:backward].in? Dir.entries(Rails.configuration.srv_logs)
                File::Tail::Logfile.open(Rails.configuration.srv_logs + params[:backward], :return_if_eof => true) do |log|
                    begin
                        while(a.length < 200)
                            if a.length == 0
                                log.tail(1) do |line|
                                    if !(line.include?("Wget") || line.include?("curl"))
                                        a.append(line)
                                        log_index = log_index + 1
                                    end
                                end
                            else
                                log.backward(log_index).tail(1) do |line|
                                    if last_one == line
                                        a.append(nil)
                                        log_index = log_index + 1
                                    else
                                        if !(line.include?("Wget") || line.include?("curl"))
                                            a.append(line)
                                            log_index = log_index + 1
                                        end
                                    end
                                    last_one = line
                                end
                            end
                        end
                    rescue File::Tail::BreakException
                        render :json => ["Made it to end of file!"]
                        return
                    end
                end
            else
                render :json => ["Incorrect Parameter"]
                return
            end
        end
        
        render :json => {location: log_index, logs: a}
    end

    
    protected

    def log_file_params
        params.require(:log_file).permit(:backward, :location)
    end

end
