class CompositionalRoleVariablesController < ApplicationController
    before_action :authenticate_admin!

    def show
    end

    def env_vars
        render :json => YAML.load_file('storage/ansible/environment/group_vars/compositional/all.yml').to_json
    end

    def update
        storage_root = "#{Rails.application.config.storage.root}"
        file_path = "#{storage_root}/ansible/environment/group_vars/compositional/all.yml"
        all_env_vars = YAML.load_file(file_path)
        params['compositional_role_variable'].each do |param_key, param_val|
            all_env_vars[param_key] = param_val
        end
        File.open("#{file_path}", 'w+') do |f|
            f.write(all_env_vars.to_yaml)
        end
        update_succeeded = true
        unless ENV['OURCOMPOSEBOT_RO_KEY'].blank?
          post_response = Rundeck.post_update_environment_repo
          get_response = Rundeck.get_update_environment_repo(post_response)
          unless get_response['executionState'] == 'SUCCEEDED'
            update_succeeded = false
          end
        end
        results = {}
        results['env'] = YAML.load_file('storage/ansible/environment/group_vars/compositional/all.yml')
        results['update_succeeded'] = update_succeeded
        render :json => results.to_json
    end
end
