class CustomizeController < ApplicationController
    before_action :authenticate_admin!

    def show
    end

    def data
        if Customization.first.nil?
            render json: {
                orgName: "Please Enter Data!",
                tagline: "Please Enter Data!"
            }
        else
            render json: {
                orgName: Customization.first.org_name,
                tagline: Customization.first.tagline
            }
        end
    end

    def update
        if Customization.first.nil?
            @update_customizations = Customization.create!(tagline: customization_params[:tagline], org_name: customization_params[:orgname])
            render json: {
                customization: @update_customizations
            }
        else
            @update_customizations = Customization.first.update!(tagline: customization_params[:tagline], org_name: customization_params[:orgname])
            render json: {
                customization: @update_customizations
            }
        end
    end

    protected

    def customization_params
        params.require(:customization).permit(:tagline, :orgname)
    end

end
