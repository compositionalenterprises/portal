class ApplicationMailer < ActionMailer::Base
  default from: 'portal@mg.ourcompose.com'
  layout 'mailer'
end
