import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"

import SidebarNav from './dashboard/sidebarNav'
import Users from './dashboard/users'
import EnvironmentVariables from './dashboard/env'
import Functions from './dashboard/functions'
import Portal from './dashboard/portal'
import Application from './dashboard/application'

export default class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            page: "dashboard",
            services: this.props.services,
            healthCheckStatus: this.props.healthCheckStatus,
        }
        this.setPage = this.setPage.bind(this)
    }
    setPage(appArray){
        if(appArray.length > 1){
            this.setState({'page': appArray[0], 'application': appArray[1]})
        }else{
            this.setState({'page': appArray[0]})
        }
    }
    componentDidMount(){
    }
    render(){
        let view;
        view = () => {switch(this.state.page){
                case 'dashboard': return <ApplicationList services={this.state.services} healthCheckStatus={this.state.healthCheckStatus}/>;
                case 'users': return <Users/>;
                case 'environment': return <EnvironmentVariables/>;
                //case 'functions': return <Functions/>; //see sidebar-nav and branch oc2318
                case 'portal': return <Portal/>;
                case 'application': {
                    return <Application application={this.state.application} key={this.state.application}/>;
                }
                default: return <ApplicationList/>
            }
        }
        return(
            <React.Fragment>
            <div className="container-fluid">
                <SidebarNav 
                    setPage={this.setPage}
                    services={this.state.services}
                />
                {view()}
            </div>
            </React.Fragment>
        )
    }
}

class ApplicationList extends Component {
    constructor(props){
        super(props);
        this.state = {
            services: this.props.services,
            healthCheckStatus: this.props.healthCheckStatus
        }
        this.runCompositionalRole = this.runCompositionalRole.bind(this)
    }
    async runCompositionalRole(e){
        e.preventDefault();
        //console.log(e.target.getAttribute('data-service-name'))
        var service = JSON.stringify({'service': e.target.getAttribute('data-service-name')})
        let response = await fetch("execution_abstraction/run_ourcompose_role", {
            method: "POST",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: service,
        })
        if (response.ok){
            let responseJSON = await response.json()
            this.setState({healthCheckStatus: {status:"Running Compositional Role"}});
        }
    }
    render(){
        let services;
        //console.log(this.props.healthCheckStatus)
        if (this.state.services != null && this.props.healthCheckStatus != null){
            if (this.props.healthCheckStatus["service_status"] != null){
                let serviceStatus = JSON.parse(this.props.healthCheckStatus["service_status"].replaceAll("=>",":"))
                //console.log(serviceStatus)
                services = this.state.services.map((service, index) => {
                    let status;
                    if (serviceStatus[service.name] != null){
                        if (serviceStatus[service.name] == "healthy"){
                            status = <span className="badge badge-success">{serviceStatus[service.name].charAt(0).toUpperCase() + serviceStatus[service.name].slice(1)}</span>
                        }else if (serviceStatus[service.name] == "unhealthy"){
                            status = <span className="badge badge-danger">{serviceStatus[service.name].charAt(0).toUpperCase() + serviceStatus[service.name].slice(1)}</span>
                        }else{
                            status = <span className="badge badge-warning">{serviceStatus[service.name].charAt(0).toUpperCase() + serviceStatus[service.name].slice(1)}</span>
                        }
                    } else {
                        status = <span className="badge badge-warning">No Health Checks Currently Available</span>
                    }
                    if (service != null){
                        return(
                            <tr key={index}>
                                <th scope="row"><a target="_blank" href={"https://" + service.url}>{service.name.charAt(0).toUpperCase() + service.name.slice(1)}</a></th>
                                <td>{status}</td>
                                <td><a href="" className="text-muted" onClick={this.runCompositionalRole} data-service-name={service.name}>Run OurCompose Role</a></td>
                            </tr>
                        )
                    }
                });
            } else {
                services = <tr><th>Rendering Applications</th></tr>
            }
        } else {
            services = <tr><th>Rendering Applications</th></tr>
        }
        return(
            <React.Fragment>
                <div className="col-md-8 ml-sm-auto col-lg-10 px-4 mt-2">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-2">
                        <h1 className="h2">Applications</h1>
                    </div>
                    <table className="table table-sm">
                    <thead>
                        <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Status (Last Health Check)</th>
                        <th scope="col">Having Issues?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {services}
                    </tbody>
                    </table>
                </div>
            </React.Fragment>
        )
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const node = document.getElementById('dashboard')
    const data = JSON.parse(node.getAttribute('data'))
    ReactDOM.render(
    <Dashboard services={data.services} healthCheckStatus={data.healthCheckStatus} />,
        node.appendChild(document.createElement('div')),
    )
})