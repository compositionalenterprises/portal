import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"

export default class Environment extends Component {
    constructor(props){
        super(props);
        this.state = {
        }
    }
    componentDidMount(){
    }
    render(){
        return(
            <React.Fragment>
                <div className="col-md-10 ml-sm-auto col-lg-10 px-4 mt-2">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-2">
                        <h1 className="h2">Instance Environment Variables <small className="text-danger">Warning changing these may break your instance</small></h1>
                    </div>
                    <Env />
                </div>
            </React.Fragment>
        )
    }
}

class Env extends Component {
    constructor(props){
        super(props);
        this.state = {
            env: null,
            update_succeeded: true,
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.saveChanges = this.saveChanges.bind(this)
    }
    componentDidMount(){
        fetch('env/env_vars')
            .then(response => response.json())
            .then(data => this.setState({env: data}));
    }
    handleInputChange(e){
        //console.log(e.target.dataset.key)
        //console.log(e.target.value)

        // This way gets us the ability to update the environment var itself with the changes.
        // However, I think what we want to do is to get the ones that changed into their own
        // dict/hash so that we can parse them later on. That should also make the "Discard Changes"
        // button easier to implement.
        //let env_items = this.state.env;
        //env_items[e.target.dataset.key] = e.target.value;

        // This way we're adding the changes to a new dict.
        let new_state = this.state;
        if (this.state.env[e.target.dataset.key] != e.target.value) {
            // Only if the key is different do we store it in the new_env_items state
            if (!('new_env_vars' in new_state)) {
                // Here we're  creating the new dict before we update it
                new_state.new_env_vars = {}
            }
            // Actually store the item in the state
            new_state.new_env_vars[e.target.dataset.key] = e.target.value;
            // TODO: In the case that the value was erased entirely, remove/reset it
        } else {
            // confirm that it's no longer there
            delete new_state.new_env_vars[e.target.dataset.key]
            // Remove the entire dict if there are no new vars left in there.
            if (Object.keys(new_state.new_env_vars).length == 0) {
                delete new_state.new_env_vars
            }
        }

        this.setState(new_state);

    }
    async saveChanges(e){
        if (!('new_env_vars' in this.state)) {
            return
        }
        var newEnvVars = JSON.stringify(this.state.new_env_vars)

        let response = await fetch("env/update", {
            method: "POST",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: newEnvVars,
        })
        if (response.ok){
            let responseJSON = await response.json()
            //console.log(responseJSON)
            // Remove 'new_env_vars' key in the state hash when we're done
            delete this.state['new_env_vars']
            // We have to do this hack in the event that we don't reset the env
            // for some reason, we _always_ want this to trigger a reset
            this.setState(responseJSON);
        }
    }
    render(){
        let listItems;
        let saveButton;
        //console.log(this.state)
        if (this.state.env != null){
            listItems = Object.keys(this.state.env).map((vari) => {
                if (vari != undefined){
                    return(
                        <tr className="form-group" key={vari}>
                            <th className="col-sm-2" style={{width:'50%',textOverflow:'clip'}} scope="row">{vari}</th>
                            <td><input type="text" className="form-control" style={{width:'100%',textOverflow:'clip'}} defaultValue={this.state.env[vari]} onChange={this.handleInputChange} autoComplete="off" data-key={vari}/></td>
                        </tr>
                    )
                }
            });
        } else {
            listItems = <tr><th>Data hasn't rendered yet, or no editable variables found!</th></tr>
        }
        if (this.state.update_succeeded) {
            saveButton = <button onClick={this.saveChanges} className="btn btn-success mb-3">Save Changes</button>
        } else {
            saveButton = <button className="btn btn-danger mb-3">FAILED TO SAVE! Please refresh the page...</button>
        }
        return(<React.Fragment>
            <table className="table">
                <tbody>
                    {listItems}
                </tbody>
            </table>
            <div className="form-row align-items-center">
                <div className="form-group col-sm-8">
                    {saveButton}
                </div>
                <div className="form-group col-sm-8">
                    <button onClick={this.discardChanges} className="btn btn-warning mb-3">Discard Changes</button>
                </div>
            </div>
        </React.Fragment>)
    }
}