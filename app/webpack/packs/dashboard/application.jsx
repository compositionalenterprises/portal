import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"

import Configuration from './application/configuration'
import Logs from './application/logs'
import Users from './application/users'
import Status from './application/status'


export default class Application extends Component {
    constructor(props){
        super(props)
        this.state = {
        }
        this.setPage = this.setPage.bind(this)
    }
    setPage(appArray){
        if(appArray.length > 1){
            this.setState({'page': appArray[0], 'application': appArray[1]})
        }else{
            this.setState({'page': appArray[0]})
        }
    }
    render(){                   
        let view;
        view = () => {switch(this.state.page){
                case 'logs': return <Logs application={this.props.application}/>;
                case 'configuration': return <Configuration application={this.props.application}/>;
                //case 'users': return <Users application={this.props.application}/>; //Users are in main page
                case 'status': return <Status application={this.props.application}/>;
                default: return <Status application={this.props.application}/>;
            }
        }
        return(
            <React.Fragment>
                <div className="col-md-3 offset-lg-2 col-lg-10 mt-2 pt-3 mb-2">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-2 col-lg-3">
                                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                                    <h1 className="h2">{this.props.application.charAt(0).toUpperCase() + this.props.application.slice(1)}</h1>
                                </div>
                                <ApplicationSidebar setPage={this.setPage}/>
                            </div>
                            <div className="col-md-9 col-lg-9 mt-2">
                                {view()}
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export class ApplicationSidebar extends Component {
    constructor(props){
        super(props)
        this.setPage = this.setPage.bind(this)
    }
    componentDidMount(){
    }
    setPage(e){
        e.preventDefault();
        //console.log(e.target.attributes['data-page-id'].value);
        this.props.setPage([e.target.attributes['data-page-id'].value])
        this.applicationRef.current.children.forEach((element) =>
            element.children[0].classList.add('text-muted')
        )
        this.servicesRef.current.children.forEach((element) =>
            element.children[0].classList.add('text-muted')
        )
        
        e.target.classList.remove('text-muted')
        //console.log(e.target)
        // we want to send this up the chain to the main page for the dashboard to know what to render.
    }
    render(){
        return(
            <React.Fragment>
            <div className="row">
                <nav className="col-md-12 d-none d-md-block">
                    <div className="sidebar-sticky mt-2">
                        <ul className="nav flex-column">
                            <li className="nav-item">
                                <a className="nav-link text-muted" href="#" onClick={this.setPage} data-page-id="status">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#6c757d" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                                Status
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link text-muted" href="#" onClick={this.setPage} data-page-id="configuration">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#6c757d" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect><rect x="9" y="9" width="6" height="6"></rect><line x1="9" y1="1" x2="9" y2="4"></line><line x1="15" y1="1" x2="15" y2="4"></line><line x1="9" y1="20" x2="9" y2="23"></line><line x1="15" y1="20" x2="15" y2="23"></line><line x1="20" y1="9" x2="23" y2="9"></line><line x1="20" y1="14" x2="23" y2="14"></line><line x1="1" y1="9" x2="4" y2="9"></line><line x1="1" y1="14" x2="4" y2="14"></line></svg>
                                Configuration
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link text-muted" href="#" onClick={this.setPage} data-page-id="logs">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#6c757d" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><path d="M13 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V9l-7-7z"/><path d="M13 3v6h6"/></svg>
                                Logs
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            </React.Fragment>
        )
    }
}

