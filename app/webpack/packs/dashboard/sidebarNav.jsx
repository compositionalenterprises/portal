
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"

export default class SidebarNav extends Component {
    constructor(props){
        super(props)
        this.state = {
            services: this.props.services,
        }
        this.servicesRef = React.createRef()
        this.applicationRef = React.createRef()
        this.setPage = this.setPage.bind(this)
    }
    setPage(e){
        e.preventDefault();
        //console.log(e.target.attributes['data-page-id'].value);
        if (e.target.attributes['data-application-id'] != null){
            this.props.setPage([e.target.attributes['data-page-id'].value, e.target.attributes['data-application-id'].value])
        }else{
            this.props.setPage([e.target.attributes['data-page-id'].value])
        }
        this.applicationRef.current.children.forEach((element) =>
            element.children[0].classList.add('text-muted')
        )
        this.servicesRef.current.children.forEach((element) =>
            element.children[0].classList.add('text-muted')
        )
        
        e.target.classList.remove('text-muted')
        //console.log(e.target)
        // we want to send this up the chain to the main page for the dashboard to know what to render.
    }
    render(){
        let services;
        if (this.state.services != null){
            services = this.state.services.map((service, index) => {
                if (service != null){
                    return(
                        <li className="nav-item" key={index}>
                            <a className="nav-link text-muted" onClick={this.setPage} data-page-id="application" href="#" data-application-id={service.name} key={index}>
                                <svg key={index} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                {service.name.charAt(0).toUpperCase() + service.name.slice(1)}
                            </a>
                        </li>
                    )
                }
            });
        } else {
            services = <li className="nav-item">No Applications</li>
        }
        return(
            <React.Fragment>
                <div className="row">
                    <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                        <div className="sidebar-sticky mt-2">
                            <ul className="nav flex-column" ref={this.applicationRef}>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" onClick={this.setPage} data-page-id="dashboard">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                    Dashboard
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-muted" href="#" onClick={this.setPage} data-page-id="users">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                                    Users
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-muted" href="#" onClick={this.setPage} data-page-id="environment">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                                    Enivronment
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-muted" href="#" onClick={this.setPage} data-page-id="portal">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" stroke="currentColor" strokeWidth="1" strokeLinecap="round" strokeLinejoin="round" className="feather feather-layers" className="bi bi-gear" viewBox="0 0 20 20"><path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/><path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/></svg>
                                    Portal
                                    </a>
                                </li>
                            </ul>
                            <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Applications</span>
                            </h6>
                            <ul className="nav flex-column mb-2" ref={this.servicesRef}>
                                {services}
                            </ul>
                        </div>
                    </nav>
                </div>
            </React.Fragment>
        )
    }
}