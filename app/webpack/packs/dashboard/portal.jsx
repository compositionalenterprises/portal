import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"

export default class Portal extends Component {
    constructor(props){
        super(props);
        this.state = {
            tagline: "",
            orgName: "",
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.updateCustomizations = this.updateCustomizations.bind(this)
    }
    componentDidMount(){
        fetch('customize/data')
            .then(response => response.json())
            .then(data => this.setState({tagline: data.tagline, orgName: data.orgName}));
    }
    async updateCustomizations(e){
        e.preventDefault();
        let sendIt = null;
        sendIt = JSON.stringify({
            customization: {
                tagline: this.state.tagline,
                orgname: this.state.orgName,
            }
        })
        let response = await fetch("customize", {
            method: "PUT",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: sendIt
        });
        if (response.ok){
            let responseJson = await response.json()
            if(responseJson.customization == true){
                this.setState({just_updated: true})
                setTimeout(function(){
                    this.setState({just_updated:false});
                }.bind(this),3000); 
            }
        }
    }
    handleInputChange(e){
        this.setState({[e.target.id]: e.target.value})
    }
    render(){
        let button = <button onClick={this.updateCustomizations} className="btn btn-primary mt-3 col-sm-2">Update Information</button>
        if(this.state.just_updated){
            button = <button onClick={this.updateCustomizations} className="btn btn-success mt-3 col-sm-2">Update Information</button>
        }
        return(
            <React.Fragment>
                <div className="col-md-10 ml-sm-auto col-lg-10 px-4 mt-2">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-2">
                        <h1 className="h2">Customize Portal</h1>
                    </div>
                    <form>
                        <div className="form-group">
                            <label htmlFor="orgName">Org Name</label>
                            <input type="text" className="form-control col-md-4 mb-1" id="orgName" value={this.state.orgName} autoComplete="off" onChange={this.handleInputChange}/>
                        
                            <label htmlFor="tagline">Tagline</label>
                            <input type="text" className="form-control col-md-4" id="tagline" value={this.state.tagline} autoComplete="off" onChange={this.handleInputChange}/>
                            
                            {button}
                        </div>
                    </form>
                </div>
            </React.Fragment>
        )
    }
}