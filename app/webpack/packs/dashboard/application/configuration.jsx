import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"


export default class ApplicationConfiguration extends Component{
    constructor(props){
        super(props)
        this.state = {
            env: null,
            update_succeeded: true
        }
        this.saveChanges = this.saveChanges.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    
    componentDidMount(){
        var url = "execution_abstraction/env_configuration?service=" + this.props.application
        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({'env': data})
            .then(console.log(data)));
    }

    async saveChanges(e){
        e.preventDefault();
        var data = JSON.stringify({
                service: this.props.application, 
                yml_vars: this.state.env
        })
        let response = await fetch("execution_abstraction/env_configuration", {
            method: "POST",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: data,
        })
        if (response.ok){
            let responseJSON = await response.json()
            this.setState({response: {status: responseJSON}});
        }
    }

    handleInputChange(e){
        // console.log(e.target.dataset.key)
        // console.log(e.target.value)

        let elements = this.state.env
        if(elements[e.target.dataset.key] != e.target.value){
            elements[e.target.dataset.key] = e.target.value
        }
        this.setState({'env':
            elements
        })
    }

    render(){
        let listItems;
        let saveButton;
        if (this.state.env != null){
            listItems = Object.keys(this.state.env).map((vari) => {
                if (vari != undefined){
                    return(
                        <tr className="form-group" key={vari}>
                            <th className="col-sm-2" style={{width:'50%',textOverflow:'clip'}} scope="row">{vari}</th>
                            <td><input type="text" className="form-control" style={{width:'100%',textOverflow:'clip'}} defaultValue={this.state.env[vari]} onChange={this.handleInputChange} autoComplete="off" data-key={vari}/></td>
                        </tr>
                    )
                }
            });
        } else {
            listItems = <tr><th>Data hasn't rendered yet, or no editable variables found!</th></tr>
        }
        if (this.state.update_succeeded) {
            saveButton = <button onClick={this.saveChanges} className="btn btn-success mb-3">Save Changes</button>
        } else {
            saveButton = <button className="btn btn-danger mb-3">FAILED TO SAVE! Please refresh the page...</button>
        }
        return(
            <React.Fragment>
            <table className="table">
                <tbody>
                    {listItems}
                </tbody>
            </table>
            <div className="form-row align-items-center">
                <div className="form-group col-sm-8">
                    {saveButton}
                </div>
            </div>
            </React.Fragment>
        // environment variables for the service
        // parse out service name from environment to show in configuration view
        // showing all available variables
        //   - parsing the version of the collection
        //   - git clone of the collection to look at the defaults main.yml in roles for service

        // TODO
        // parsing vault passwords     
        )
    }
}

