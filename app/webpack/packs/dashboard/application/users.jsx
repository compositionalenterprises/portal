import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"


export default class Users extends Component {
    constructor(props){
        super(props)
    }

    render(){
        return(
        <React.Fragment>
            <div>Users</div>
            <AddUser application={this.props.application} />
        </React.Fragment>
        )
    }
}

export class ListUsers extends Component{
    // Show current users in application
    constructor(props){
        super(props)
    }
    render(){
        return(
            <React.Fragment>
                <div>Show Users/Remove Users</div>
            </React.Fragment>
        )
    }
}

export class AddUser extends Component{
    // Add User
    constructor(props){
        super(props)
    }
    render(){
        return(
            <React.Fragment>
                <div>Add User</div>
            </React.Fragment>
        )
    }
}
