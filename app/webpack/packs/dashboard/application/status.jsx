import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"
import { render } from 'sass'


export default class Status extends Component{
    constructor(props){
        super(props)
        this.state = {
            application: this.props.application,
        }
    }
    render(){
        return(
            <React.Fragment>
                <Badge application={this.props.application} />
                <ApplicationStatus application={this.props.application} />
                <StartStopRestart application={this.props.application} />
            </React.Fragment>
        )
    }
}

export class Badge extends Component{
    //Badge for whether or not application is healthy/unhealthy
    constructor(props){
        super(props)
        this.state = {
            serviceStatus: ""
        }
    }
    componentDidMount(){
        fetch('execution_abstraction/health_check')
            .then(response => response.json())
            .then(data => this.setState({serviceStatus: data.service_status}));
    }
    render(){
        let badge = <div>Badge!</div>
        if(this.state.serviceStatus[this.props.application] != null){
            //console.log(this.state.serviceStatus[this.props.application])
            if (this.state.serviceStatus[this.props.application] == "healthy"){
                badge = <div className="badge badge-success">Healthy</div>
            }else if (this.state.serviceStatus[this.props.application] == "unhealthy"){
                badge = <div className="badge badge-danger">Unhealthy</div>
            }else{
                badge = <div className="badge badge-warning">Unknown</div>
            }
        }
        return(
            <React.Fragment>
                <div>Current Application Status</div>
                {badge}
            </React.Fragment>
        )
    }
}

export class ApplicationStatus extends Component{
    //Badge for whether or not application is healthy/unhealthy
    constructor(props){
        super(props)
        this.state = {
            services: ""
        }
    }
    componentDidMount(){
        fetch('execution_abstraction/service_state')
            .then(response => response.json())
            .then(data => this.setState({services: data.ourcompose_common_services}));
    }
    render(){
        //console.log(this.state.services)
        let badge = <div>Badge!</div>
        if (this.state.services.includes(this.props.application) || this.props.application == "portal"){
            badge = <div className="badge badge-success">Present</div>
        }else{
            badge = <div className="badge badge-danger">Absent</div>
        }
        return(
            <React.Fragment>
                <div>Current Application State</div>
                {badge}
            </React.Fragment>
        )
    }
}

export class StartStopRestart extends Component{
    // Buttons for starting stopping restarting 
    // TODO: Tooltips
    constructor(props){
        super(props)
        this.actionService = this.actionService.bind(this)
        this.state = {
            response: {status: {action: null}}
        }
    }
    async actionService(e){
        //console.log("Running " + e.target.getAttribute('data-service-action') +" on " + e.target.getAttribute('data-service-name'))
        e.preventDefault();
        //console.log(e.target.getAttribute('data-service-name'))
        var service = JSON.stringify({'service': e.target.getAttribute('data-service-name'), 'service_action': e.target.getAttribute('data-service-action')})
        let response = await fetch("execution_abstraction/service_state", {
            method: "POST",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: service,
        })
        if (response.ok){
            let responseJSON = await response.json()
            this.setState({response: {status: responseJSON}});
        }
    }
    render(){
        let startButton = <button className="btn btn-primary mr-2" onClick={this.actionService} data-service-name={this.props.application} data-service-action="start">Start</button>  
        let stopButton = <button className="btn btn-danger mr-2" onClick={this.actionService} data-service-name={this.props.application} data-service-action="stop">Stop</button>  
        let restartButton = <button className="btn btn-warning" onClick={this.actionService} data-service-name={this.props.application} data-service-action="restart">Restart</button> 
        let allButtons = <div></div>
        if(this.state.response.status.action == "start"){
            allButtons = <div>{stopButton}</div>
        }else if(this.state.response.status.action == "stop"){
            allButtons = <div>{startButton}{restartButton}</div>
        }else if(this.state.response.status.action == "restart"){
            allButtons = <div>{stopButton}</div>
        }else{
            allButtons = <div>{startButton}{stopButton}{restartButton}</div>
        }
        return(
            <React.Fragment>
                <div className="mt-4">
                    <div>Application Options</div>
                    {allButtons}
                </div>
            </React.Fragment>
        )
    }
}