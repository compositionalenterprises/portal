import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"


export default class Logs extends Component{
    constructor(props){
        super(props)
        this.state = {
            logs: this.props.logs,
            location: 0,
            logfileLength: 0,
        }
        this.handleClick = this.handleClick.bind(this);
        this.changeLogFile = this.changeLogFile.bind(this);
        this.loadMoreLogs = this.loadMoreLogs.bind(this);
    }
    componentDidMount(){
        fetch('logs/available')
            .then(response => response.json())
            .then(data => this.setState({'available': data}));
        fetch('logs/logfile_length')
            .then(response => response.json())
            .then(data => this.setState({'logfileLength': data.length}))
        //console.log(this.state.logfileLength)
    }
    changeLogFile(file){
        fetch('logs/get_file' + "?backward=" + file)
            .then(response => response.json())
            .then(data => this.setState({logs: data.logs, title: file, logLocation: 0}));
        fetch('logs/logfile_length' + "?backward=" + file)
            .then(response => response.json())
            .then(data => this.setState({'logfileLength': data.length}))
    }
    loadMoreLogs(location){
        fetch('logs/get_file' + "?backward=" + this.state.logFile + "&location=" + location)
            .then(response => response.json())
            .then(data => this.setState({logs: data.logs, title: this.state.logFile, logLocation: data.location}));
    }
    handleClick = (e, vari) => {
        e.preventDefault();
        this.setState({logFile: vari})
        this.changeLogFile(vari)
    };
    render(){
        let alistItems;
        //let title = this.state.title.substring(0,this.state.title.indexOf(".log"))
        //title = title.split("_").map(element => element[0].toUpperCase() + element.slice(1)).join().replaceAll(",", " ")

        if (this.state.available != null){
            alistItems = this.state.available.sort().map((vari) => {
                if (vari != null){
                    if (!(vari == ".." || vari == '.' || !vari.startsWith(this.props.application))){
                        //console.log(vari + " " + vari.startsWith(this.props.application))
                        let showLogName = vari.substring(0,vari.indexOf(".log"))
                        showLogName = showLogName.split("_").map(element => element[0].toUpperCase() + element.slice(1)).join().replaceAll(",", " ")
                        return(
                            <div style= {{borderTop: "1px solid black", borderLeft: "1px solid black", borderRight: "1px solid black", borderTopLeftRadius: "20%", borderTopRightRadius: "20%"}} className="p-1">
                                <a href='#' key={vari} onClick={(e)=>this.handleClick(e,vari)}>{showLogName}</a>
                            </div>
                        )
                    } else {
                        alistItems = <div>None available!</div>
                    }
                }
            });
        } else {
            alistItems = <div>Currently Rendering!</div>
        }
        return(
            <React.Fragment>

                <div className="row flex-row col-sm-12">
                    {alistItems}
                </div>
                <LogView logs={this.state.logs} loadMoreLogs={this.loadMoreLogs} logfileLength={this.state.logfileLength}/>
            </React.Fragment>
        )
    }
}

export class ServiceLogs extends Component {
    constructor(props){
        super(props)
    }
    render(){
        return(<div></div>)
    }

    // call out to get service logs execution abstraction controller
    // wait for write to disk at the bindmounpoint (portal storage from service_logs folder)
    // list of args
    //   - options: tail, since, grep, timestamps, details, 
    // tooltips for options
    // https://docs.docker.com/engine/reference/commandline/logs/

}

export class LogView extends Component{
    constructor(props){
        super(props)
        this.state = {

        }
        this.loadMoreLogs = this.loadMoreLogs.bind(this);
    }
    componentDidMount(){
    }
    loadMoreLogs(e, location){
        e.preventDefault()
        this.props.loadMoreLogs(location)
    }
    render(){
        let logs;
        if (this.props.logs != null){
            logs = this.props.logs.slice(1).map((vari, index) => {
                if (vari != null){
                    return(
                        <tr className="" style={{fontSize: 11 + "px"}} key={index}>
                            <td key={index}>{vari}</td>
                        </tr>
                    )
                }
            });
        } else {
            logs = <tr><th></th></tr>
        }
        let loadMore, pages, pagesArray;
        pages = null;
        loadMore = null;
        if(this.props.logs != null){
            //console.log(this.props.logfileLength)
            pages = parseInt(Math.ceil(this.props.logfileLength / 200))
            if(pages > 1){
                pagesArray = Array(pages).fill().map((x,i) => {
                    if(i == 0){
                        return(
                            <a href='#' className="mr-1" onClick={(e) => this.loadMoreLogs(e, (i*200))}>Page {i+1}</a>

                        )
                    }else{
                        return(
                            <a href='#' className="mr-1" onClick={(e) => this.loadMoreLogs(e, (i*200))}>{i+1}</a>
                        )                    
                    }
                })
            }
            loadMore = <a href="#" >Load Next Page of Logs</a>
        }

        return(
            <React.Fragment>
                {logs}
                {pagesArray}
                <div className="mb-5"></div>
            </React.Fragment>
        )
    }
}
