import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"

export default class Users extends Component {
    constructor(props){
        super(props);
        this.state = {
            admins: [],
        }
        this.addAdmin = this.addAdmin.bind(this)
        this.removeAdmin = this.removeAdmin.bind(this)
    }
    componentDidMount(){
        fetch('admins')
            .then(response => response.json())
            .then(data => this.setState({admins: data.admins}));
    }
    addAdmin(admin){
        this.setState({admins: [...this.state.admins, admin]})
    }
    removeAdmin(id){
        let newAdmins = this.state.admins
        delete newAdmins[id]
        this.setState({admins: newAdmins})
    }
    render(){
        return(
            <React.Fragment>
                <div className="col-md-10 ml-sm-auto col-lg-10 px-4 mt-2">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-2">
                        <h1 className="h2">Portal Admins</h1>
                    </div>
                    <div>
                        <CurrentAdmins 
                            admins={this.state.admins}
                            removeAdmin={this.removeAdmin}
                        />
                        <AdminForm
                            addAdmin={this.addAdmin} 
                        />
                </div>
                </div>
            </React.Fragment>
        )
    }
}

class CurrentAdmins extends Component {
    constructor(props){
        super(props);
        this.state = {
        }
        this.removeAdmin = this.removeAdmin.bind(this)
    }
    componentDidUpdate(){
    }
    async removeAdmin(e){
        e.preventDefault();
        var adminIdJson = JSON.stringify({id: e.target.attributes['data-admin-id'].value})
        var indexId = e.target.attributes['data-index-id'].value
        let response = await fetch("admins", {
            method: "DELETE",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: adminIdJson
        });
        if (response.ok){
            this.props.removeAdmin(indexId)
        }
    }
    render(){
        let listItems;
        if(this.props.admins != [] || this.props.admins == undefined){
            listItems = this.props.admins.map((admin, index) => {
                if (admin != undefined){
                    return(
                        <tr data-admin-id={admin.id} className="mb-1 mt-1" key={admin.id}>
                            <th className="col-sm-10" scope="row">{admin.email}</th>
                            <td><button onClick={this.removeAdmin} data-admin-id={admin.id} data-index-id={index} className="btn btn-danger">Remove Admin</button></td>
                        </tr>
                    )
                }
            });
        }
        return(
            <div className="">
                <table className="table table-striped"><tbody>{listItems}</tbody></table>
            </div>
        )
    }
}

class AdminForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            users: null,
            inputEmail: null
        }
        this.addAdmin = this.addAdmin.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    async addAdmin(e){
        e.preventDefault();
        let sendIt = null;
        sendIt = JSON.stringify({
            admin: {
                email: this.state.inputEmail,
            }
        })
        let response = await fetch("admins", {
            method: "POST",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: sendIt
        });
        if (response.ok){
            let responseJSON = await response.json()
            this.props.addAdmin(responseJSON.admin);
        }
    }
    handleInputChange(e){
        this.setState({[e.target.id]: e.target.value})
    }
    render(){
        return (
            <React.Fragment>
                <form>
                    <div className="form-row align-items-center">
                        <div className="form-group col-sm-4">
                            <input type="text" className="form-control" id="inputEmail" placeholder="email" autoComplete="off" onChange={this.handleInputChange}/>
                        </div>
                        <div className="col-auto">
                            <button onClick={this.addAdmin} className="btn btn-primary mb-3">Add Portal Admin</button>
                        </div>
                    </div>
                </form>
            </React.Fragment>
        )
    }
}
