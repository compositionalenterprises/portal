import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"

export default class Logs extends Component {
    constructor(props){
        super(props);
        this.state = {
            logs: null,
            title: "portal_packs_access.log",
            page_number: 0
        }
        this.changeLogFile = this.changeLogFile.bind(this);
    }
    componentDidMount(){
        var backward = "portal_packs_access.log"
        fetch('logs/get_file' + "?backward=" + backward)
            .then(response => response.json())
            .then(data => this.setState({logs: data}));
    }
    changeLogFile(file){
        fetch('logs/get_file' + "?backward=" + file)
            .then(response => response.json())
            .then(data => this.setState({logs: data, title: file}));
    }
    render(){
        let listItems;
        if (this.state.logs != null){
            listItems = this.state.logs.map((vari, index) => {
                if (vari != null){
                    return(
                        <tr className="" style={{fontSize: 11 + "px"}} key={index}>
                            <td key={index}>{vari}</td>
                        </tr>
                    )
                }
            });
        } else {
            listItems = <tr><th>Log files Rendering!</th></tr>
        }
        let title = this.state.title.substring(0,this.state.title.indexOf(".log"))
        title = title.split("_").map(element => element[0].toUpperCase() + element.slice(1)).join().replaceAll(",", " ")
        return(
            <React.Fragment>
                <div className="col-md-10 ml-sm-auto col-lg-10 px-4 mt-2">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-2">
                        <h1 className="h2">Logs</h1>
                    </div>
                    {<Links changeLogFile={this.changeLogFile}></Links>}
                    <table className="mt-2">
                        <thead><tr><td><h4>{title}</h4></td></tr></thead>
                        <tbody>
                            {listItems}
                        </tbody>
                    </table>
                </div>
            </React.Fragment>
        )
    }
}

export class Links extends Component {
    constructor(props){
        super(props);
        this.state = {
            available: null
        }
        this.handleClick = this.handleClick.bind(this);
    }
    componentDidMount(){
        fetch('logs/available')
            .then(response => response.json())
            .then(data => this.setState({available: data}));
    }
    handleClick = (e, vari) => {
        e.preventDefault();
        this.props.changeLogFile(vari)
    };
    render(){
        let alistItems;
        //console.log(this.state.available)
        if (this.state.available != null){
            alistItems = this.state.available.sort().map((vari) => {
                if (vari != null){
                    if (!(vari == ".." || vari == '.')){
                        let showLogName = vari.substring(0,vari.indexOf(".log"))
                        showLogName = showLogName.split("_").map(element => element[0].toUpperCase() + element.slice(1)).join().replaceAll(",", " ")
                        return(
                            <div style= {{borderTop: "1px solid black", borderLeft: "1px solid black", borderRight: "1px solid black", borderTopLeftRadius: "20%", borderTopRightRadius: "20%"}} className="p-1">
                                <a href='#' key={vari} onClick={(e)=>this.handleClick(e,vari)}>{showLogName}</a>
                            </div>
                        )
                    }
                }
            });
        } else {
            alistItems = <tr><th>Loading available files!</th></tr>
        }
        return(<React.Fragment>
            <div className="row flex-row col-sm-12">
                    {alistItems}
            </div>
        </React.Fragment>)
    }
}
