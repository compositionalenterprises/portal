import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import "core-js"
export default class Functions extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentlyMigrating: false,
        }
        this.runMigration = this.runMigration.bind(this)
    }
    componentDidMount(){
        fetch("rundeck/migration_status")
            .then(response => response.json())
            .then(data => this.setState({currentlyMigrating: data.status, message: data.message}));
    }
    async runMigration(e){
        e.preventDefault();
        let response = await fetch("rundeck/migrate", {
            method: "POST",
            headers: {'Accept': "application/json", "Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
        })
        if (response.ok){
            let responseJSON = await response.json()
            this.setState({currentlyMigrating: true});
        }
    }
    render(){
        let button;
        if(this.state.currentlyMigrating){
            button = <button onClick={this.runMigration} className="btn btn-warning mb-3">Currently Migrating Instance</button>
        }else{
            button = <button onClick={this.runMigration} className="btn btn-primary mb-3">Migrate Instance</button>
        }
        return(   
            <React.Fragment>
                <div className="col-md-10 ml-sm-auto col-lg-10 px-4 mt-2">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-2">
                        <h1 className="h2">Compositional Functions</h1>
                    </div>
                    <div>
                        {button}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}