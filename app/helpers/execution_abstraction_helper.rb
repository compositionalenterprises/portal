module ExecutionAbstractionHelper

    def self.start_stop_restart_service(location='rundeck', service=nil, action=nil)
        if service == nil || action == nil
            raise "Error, serivce and action can't be nil!"
        end
        case location
        when 'local'
            ## TODO 
            ## Restart service locally
        when 'rundeck'
            if service == "portal"
                Rundeck.run_ourcompose_role(service='portal')
            else
                Rundeck.start_stop_restart_service(service, action)
            end
        else
            raise "Error, Invalid location"
        end
    end

    def self.current_env_vars(service_being_updated)
        service = service_being_updated
        collection_version = 'stable-4.0'
        uri = URI("https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/raw/#{collection_version}/roles/#{service}/defaults/main.yml")
        request = Net::HTTP::Get.new(uri)
        request.content_type = "application/yml"
        response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme) do |http|
            http.request(request)
        end
        response_yml = YAML.load(response.body)

        ## TODO: Blacklist of Keys that can't be shown or updated to the user
        response_yml.delete('ourcompose_portal_production_key')
        response_yml.delete('ourcompose_portal_production_yml_enc')

        # Updates default keys that already have a variable from the environment
        environment_variables = YAML.load_file('storage/ansible/environment/group_vars/compositional/all.yml')
        environment_variables.each do |key,value|
            if response_yml[key] != nil
                response_yml[key] = value
            end
        end

        return response_yml
    end

    def self.run_ourcompose_role(location='rundeck', service=nil)
        if service == nil
            raise "Error, serivce can't be nil!"
        end
        case location
        when 'local'
            ## TODO 
            ## Run OurCompose Role Locally
        when 'rundeck'
            Rundeck.run_ourcompose_role(service=service)
        else
            raise "Error, Invalid location"
        end
    end

    def self.run_health_report(location='local')
        health_check = nil
        case location
        when 'local'
            health_check = CommandsReceivable.check_health()
        when 'rundeck'
            if self.instance_has_rundeck_api_key?
                Rundeck.check_health()
            else
                raise "Error! No Rundeck Key"
            end
        else
            raise "Error, invalid method selected"
        end
    end

    def self.run_migration(location='rundeck')
        migration = nil
        case location
        when 'local'
            raise "Error, not CE supported"
        when 'rundeck'
            if self.instance_has_rundeck_api_key?
                Rundeck.migrate_instance()
            else
                raise "Error! No Rundeck Key"
            end
        else
            raise "Error, invalid method selected"
        end
    end

    def self.run_compositional_role(location='rundeck')
        run_comp_role = nil
        case location
        when 'local'
            raise "Error, not implemented"
        when 'rundeck'
            if self.instance_has_rundeck_api_key?
                Rundeck.post_compositional_role()
            else
                raise "Error! No Rundeck Key"
            end
        else
            raise "Error, invalid method selected"
        end
    end

    def self.run_backup(location='rundeck')
        run_backup = nil
        case location
        when 'local'
            raise "Error, not implemented"
        when 'rundeck'
            if self.instance_has_rundeck_api_key?
                Rundeck.full_backup()
            else
                raise "Error! No Rundeck Key"
            end
        else
            raise "Error, invalid method selected"
        end
    end

    def self.instance_has_rundeck_api_key?
        return !RundeckKey.last_api_token.blank?
    end

end
