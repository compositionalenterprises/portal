class Service < ApplicationRecord
    SERVICES = [
        'akaunting',
        'bitwarden',
        'bookstack',
        'firefly',
        'jekyll',
        'kanboard',
        'manager',
        'nextcloud',
        'rundeck',
        'suitecrm',
        'wordpress'
    ]

    def disable
        self.update(enabled: false)
    end

end
