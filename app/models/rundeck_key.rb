class RundeckKey < ApplicationRecord
    def rundeck_api_token
        salt = Rails.application.credentials.encryption[:salt]
        key = ActiveSupport::KeyGenerator.new(Rails.application.credentials.encryption[:key]).generate_key(salt, 32)
        crypt = ActiveSupport::MessageEncryptor.new(key)
        if self.read_attribute(:rundeck_api_token) != nil
          return crypt.decrypt_and_verify(self.read_attribute(:rundeck_api_token))
        else
          return nil
        end
    end

    def rundeck_api_token=(val)
        if (val.nil?)
            self.write_attribute(:rundeck_api_token, nil)
        elsif (val == "")
            self.write_attribute(:rundeck_api_token, nil)
        else
            salt = Rails.application.credentials.encryption[:salt]
            key = ActiveSupport::KeyGenerator.new(Rails.application.credentials.encryption[:key]).generate_key(salt, 32)
            crypt = ActiveSupport::MessageEncryptor.new(key)
            encrypted_vp = crypt.encrypt_and_sign(val)
            self.write_attribute(:rundeck_api_token, encrypted_vp)
        end
    end

    def vault_password
        salt = Rails.application.credentials.encryption[:salt]
        key = ActiveSupport::KeyGenerator.new(Rails.application.credentials.encryption[:key]).generate_key(salt, 32)
        crypt = ActiveSupport::MessageEncryptor.new(key)
        if self.read_attribute(:vault_password) != nil
          return crypt.decrypt_and_verify(self.read_attribute(:vault_password))
        else
          return nil
        end
    end

    def vault_password=(val)
        if (val.nil?)
            self.write_attribute(:vault_password, nil)
        elsif (val == "")
            self.write_attribute(:vault_password, nil)
        else
            salt = Rails.application.credentials.encryption[:salt]
            key = ActiveSupport::KeyGenerator.new(Rails.application.credentials.encryption[:key]).generate_key(salt, 32)
            crypt = ActiveSupport::MessageEncryptor.new(key)
            encrypted_vp = crypt.encrypt_and_sign(val)
            self.write_attribute(:vault_password, encrypted_vp)
        end
    end

    def self.last_api_token(n=0)
        n = n
        if self.order(id: :desc).limit(1).offset(n).first.rundeck_api_token != nil
            return self.order(id: :desc).limit(1).offset(n).first.rundeck_api_token
        else
            while self.order(id: :desc).limit(1).offset(n).first.rundeck_api_token == nil
                n = n + 1
                if self.order(id: :desc).limit(1).offset(n).first.rundeck_api_token != nil
                    return self.order(id: :desc).limit(1).offset(n).first.rundeck_api_token
                end
            end
        end
        return nil
    rescue NoMethodError
        puts "Reached end of loop and tried to pull token where it does not exist, returning nil"
        return nil
    rescue ActiveSupport::MessageVerifier::InvalidSignature
        puts "Invalid Signature, Likely due to rails attempting to load these from cache"
        puts "Likely a big time issue if you are passing the wrong keys in.."
        puts "Beware of silent errors."
        return nil
    end

    def self.last_vault_password(n=0)
        n = n
        if self.order(id: :desc).limit(1).offset(n).first.vault_password != nil
            return self.order(id: :desc).limit(1).offset(n).first.vault_password
        else
            while self.order(id: :desc).limit(1).offset(n).first.vault_password == nil
                n = n + 1
                if self.order(id: :desc).limit(1).offset(n).first.vault_password != nil
                    return self.order(id: :desc).limit(1).offset(n).first.vault_password
                end
            end
        end
        return nil
    rescue NoMethodError
        puts "Reached end of loop and tried to pull key where it does not exist, returning nil"
        return nil
    end

    def self.last_base_url(n=0)
        n = n
        if self.order(id: :desc).limit(1).offset(n).first.base_url != nil
            return self.order(id: :desc).limit(1).offset(n).first.base_url
        else
            while self.order(id: :desc).limit(1).offset(n).first.base_url == nil
                n = n + 1
                if self.order(id: :desc).limit(1).offset(n).first.base_url != nil
                    return self.order(id: :desc).limit(1).offset(n).first.base_url
                end
            end
        end
        return nil
    rescue NoMethodError
        puts "Reached end of loop and tried to pull last base url where it does not exist, returning nil"
        return nil
    rescue ActiveSupport::MessageVerifier::InvalidSignature
        puts "Invalid Signature, Likely due to rails attempting to load these from cache"
        puts "Likely a big time issue if you are passing the wrong keys in.."
        puts "Beware of silent errors."
        return nil
    end

    def self.last_role_branch(n=0)
        n = n
        if self.order(id: :desc).limit(1).offset(n).first.role_branch != nil
            return self.order(id: :desc).limit(1).offset(n).first.role_branch
        else
            while self.order(id: :desc).limit(1).offset(n).first.role_branch == nil
                n = n + 1
                if self.order(id: :desc).limit(1).offset(n).first.role_branch != nil
                    return self.order(id: :desc).limit(1).offset(n).first.role_branch
                end
            end
        end
        return "master"
    rescue NoMethodError
        return "master"
    end


end
