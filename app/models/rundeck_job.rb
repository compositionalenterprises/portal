class RundeckJob < ApplicationRecord
    def self.valid_to_migrate?
        if self.where(job_id: Rundeck.migrate_instance_job_id).count == 0
            return true
        else
            if ((Time.now - self.where(job_id: Rundeck.migrate_instance_job_id).last.created_at) < 1.day.ago.to_f)
                return false
            else
                return true
            end
        end
    end

    def self.valid_to_backup?
        if self.where(job_id: Rundeck.full_backup_job_id).count == 0
            return true
        else
            if ((Time.now - self.where(job_id: Rundeck.full_backup_job_id).last.created_at) < 1.day.ago.to_f)
                return false
            else
                return true
            end
        end
    end

    def self.last_health_report_id
        if self.where(job_name:"health_report").count == 0
          return nil
        else
          if !self.where(job_name:"health_report").where.not(rundeck_job_id: nil)[-1].blank?
            return self.where(job_name:"health_report").where.not(rundeck_job_id: nil)[-1].rundeck_job_id
          else
            return nil
          end
        end
    end

end
