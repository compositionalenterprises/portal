class AdminAlert < ApplicationRecord
    # A model built to prevent the spamming the shit out of admins

    def self.ready?
        # Fib alert.. 1,1,2,3,5,8,13
        # Structure is: 5 min, 5 min, 10 min, 15 min, 25 min, 40 min, 65 min
        # 
        fib_alert = [1,2,3,5,8,13,21,34,55,89]
        if self.last.blank?
            return true
        end
        if fib_alert.include? self.last.count
            return true
        else
            return false
        end
    end
end
