FROM ruby:2.7.0-alpine3.11 as builder

# Define basic environment variables
ENV NODE_ENV production
ENV RAILS_ENV production
ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true

# Install alpine packages
RUN apk add --no-cache \
  build-base \
  busybox \
  ca-certificates \
  cmake \
  curl \
  git \
  gnupg1 \
  graphicsmagick \
  libffi-dev \
  libsodium-dev \
  openssh-client \
  mysql-dev \
  mysql-client \
  tzdata \
  python \
  libssl1.1 \
  libuv \
  musl \
  libgcc \
  libstdc++ \
  nghttp2-libs \ 
  zlib \
  nodejs \
  npm \
  yarn


# Define WORKDIR
WORKDIR /app

# Use bunlder to avoid exit with code 1 bugs while doing integration test
RUN gem install bundler -v 2 --no-doc

# Copy dependency manifest
COPY . /app/

# Install Ruby dependenciesgit 
# RUN /usr/local/bundle/bin/bundle update --bundler
RUN bundle config set without 'development test'
RUN bundle install --jobs $(nproc) --retry 3 \
      && rm -rf /usr/local/bundle/bundler/gems/*/.git /usr/local/bundle/cache/

# Remove Older Assets
# RUN rm -rf public/packs node_modules/

# Compile production assets with sample key
RUN mv config/credentials/production.yml.enc config/credentials/production.yml.enc.bak; \
    mv config/credentials/sample.key config/credentials/production.key; \
    mv config/credentials/sample.yml.enc config/credentials/production.yml.enc

RUN yarn install

# Overwrite the overwritten defaults
COPY ./config/webpacker.yml /app/config/webpacker.yml
COPY ./config/webpack/environment.js /app/config/environment.js
COPY ./config/webpack/production.js /app/config/production.js
COPY ./babel.config.js /app/babel.config.js

# RUN RAILS_ENV=production bundle exec rails webpacker:compile
RUN RAILS_ENV=production ./bin/webpack


# Slim down Image
FROM ruby:2.7.0-alpine3.11
RUN apk add --update --no-cache \
  openssl \
  openssh-client \
  git \
  tzdata \
  mysql-dev \
  mysql-client \
  nodejs
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY --from=builder /app/ /app/
COPY --from=builder /app/public /app/public
ENV RAILS_LOG_TO_STDOUT false
WORKDIR /app
EXPOSE 3000

# Start the Server
ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb", "-e", "production"]
