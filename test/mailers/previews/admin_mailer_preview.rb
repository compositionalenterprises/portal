class AdminMailerPreview < ActionMailer::Preview
    def welcome_email
        AdminMailer.confirmation_instructions(Admin.first, "token", {password: "PASSWORD"})
    end
end
