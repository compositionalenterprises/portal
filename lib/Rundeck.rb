require 'net/http'
require 'open3'

module Rundeck

    def self.get_project_branch
        collection_version = ENV['ROLE_BRANCH']
        oc_mgmt_branch = "master"
        if collection_version.start_with?("v")
            oc_mgmt_branch = "stable-" + collection_version[1]
        elsif collection_version.start_with?('stable')
            oc_mgmt_branch = "stable-" + collection_version.split('-')[1][0]
        else
            cmd1 = "git clone https://gitlab.com/"
            cmd2 = "compositionalenterprises/"
            cmd3 = "ansible-project-ourcompose_management.git "
            project_dir = '/tmp/ansible_project'
            Open3.popen2(cmd1 + cmd2 + cmd3 + project_dir) {|stdin, stdout, status|}
            cmd1 = "git branch -a | "
            cmd2 = "grep 'remotes/origin/#{collection_version} '"
            stdin,stdout,status = Open3.popen2(cmd1 + cmd2, :chdir=>project_dir)
            Open3.popen2("rm -rf #{project_dir}") {|stdin, stdout, status|}

            if status.value.exitstatus == 0
                oc_mgmt_branch = collection_version
            else
                cmd1 = "git clone --branch #{collection_version} --depth=1 "
                cmd2 = "https://gitlab.com/compositionalenterprises/ansible-collection-"
                cmd3 = "compositionalenterprises.ourcompose.git"
                collection_dir = '/tmp/ansible_collection'
                Open3.popen2(cmd1 + cmd2 + cmd3 + " " + collection_dir) {|stdin, stdout, status|}
                cmd1 = "grep 'version:' galaxy.yml | cut -d ' ' -f 2 | cut -d '.' -f 1"
                stdin,stdout,status = Open3.popen2(cmd1, :chdir=>collection_dir)
                parsed_collection_version = stdout.read.chomp
                if parsed_collection_version == '0'
                    oc_mgmt_branch = 'master'
                else
                    oc_mgmt_branch = "stable-" + parsed_collection_version
                end
                Open3.popen2("rm -rf #{collection_dir}") {|stdin, stdout, status|}
            end
        end
        return oc_mgmt_branch
    end

    # Start/Stop/Restart Services
    def self.start_stop_restart_service(service=nil, action=nil)
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        @RundeckRoleBranch = RundeckKey.last_role_branch

        job_id = nil
        case action
        when 'restart' 
            job_id = self.restart_service_job_id
        when 'stop'
            job_id = self.stop_service_job_id
        when 'start'
            job_id = self.restart_service_job_id
        end

        project_url = "/rundeck/api/14/job/#{job_id}/run"
        base_url = self.base_server_url
        form_data = "-domain #{@RundeckCustomerBaseURL} -envvaultpass #{@RundeckVaultPassword} -branch #{@RundeckRoleBranch} -playbranch #{self.get_project_branch} -service_name #{service}"

        response = self.rundeck_post(base_url, project_url, form_data)

        json_response = JSON.parse(response)
        if json_response.has_key? 'error'
            logger = Rails.logger
            logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
            raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
        end
        RundeckJob.create(job_id: self.restart_service_job_id, job_name:"stop_start_restart", rundeck_job_id: json_response["id"])
    end

    # New RunOurCompose Role
    def self.run_ourcompose_role(service)
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        @RundeckRoleBranch = RundeckKey.last_role_branch
        run_ourcompose_role_job_id = self.run_ourcompose_role_job_id

        project_url = "/rundeck/api/14/job/#{run_ourcompose_role_job_id}/run"
        base_url = self.base_server_url
        form_data = "-domain #{@RundeckCustomerBaseURL} -envvaultpass #{@RundeckVaultPassword} -branch #{@RundeckRoleBranch} -playbranch #{self.get_project_branch} -service_name #{service}"

        response = self.rundeck_post(base_url, project_url, form_data)

        json_response = JSON.parse(response)
        if json_response.has_key? 'error'
            logger = Rails.logger
            logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
            raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
        end
        RundeckJob.create(job_id: self.run_ourcompose_role_job_id, job_name:"run_ourcompose_role", rundeck_job_id: json_response["id"])
    end

    def self.migrate_instance
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        @RundeckRoleBranch = RundeckKey.last_role_branch

        migrate_job_id = self.migrate_instance_job_id
        project_url = "/rundeck/api/14/job/#{migrate_job_id}/run"
        base_url = self.base_server_url
        form_data = "-domain #{@RundeckCustomerBaseURL} -envvaultpass #{@RundeckVaultPassword} -branch #{@RundeckRoleBranch} -playbranch #{self.get_project_branch}"

        response = self.rundeck_post(base_url, project_url, form_data)
        json_response = JSON.parse(response)
        if json_response.has_key? 'error'
            logger = Rails.logger
            logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
            raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
        end

        RundeckJob.create(job_id: self.migrate_instance_job_id, job_name:"migration", rundeck_job_id: json_response["id"])
    end

    def self.post_health_report
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        @RundeckRoleBranch = RundeckKey.last_role_branch

        health_report_job_id = self.health_report_job_id
        project_url = "/rundeck/api/14/job/#{health_report_job_id}/run"
        base_url = self.base_server_url
        form_data = "-domain #{@RundeckCustomerBaseURL} -envvaultpass #{@RundeckVaultPassword} -branch #{@RundeckRoleBranch} -playbranch #{self.get_project_branch}"

        response = self.rundeck_post(base_url, project_url, form_data)
        json_response = JSON.parse(response)
        # Log an error and raise an exception if there's an error reported in the result
        if json_response["error"] == true
            logger = Rails.logger
            logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
            raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
        end

        rundeck_job = RundeckJob.create(job_id: self.health_report_job_id, job_name:"health_report", rundeck_job_id: json_response["id"])
        return rundeck_job
    end

    def self.get_health_report
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url

        json_response = nil
        prevent_infinite_loop = 0
        while prevent_infinite_loop < 30
          # Can we confirm that this gets the correct health report id?
          project_url = "/rundeck/api/10/execution/#{RundeckJob.last_health_report_id.to_s}/state"
          base_url = self.base_server_url

          # Get the result of the run. If this errors out, we're catching all exceptions in the
          # calling function that invoked this.
          response = self.rundeck_get(base_url, project_url)
          # Same with this, if we can't JSON.parse, then that exception can get caught in the
          # calling function that invoked this.
          json_response = JSON.parse(response)
          if json_response["error"] == true
              logger = Rails.logger
              logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
              raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
          end
          break if json_response["completed"] == true
          sleep(5)
          # This variable here makes sure that we don't wait in perpetuity
          # for a completed response just in case one doesn't actually come back.
          prevent_infinite_loop = prevent_infinite_loop + 1
          # Put in error statement if we hit the limit
          if prevent_infinite_loop == 30
            logger = Rails.logger
            logger.error "Did not receive a completed response within the timeout. Instead got:\n\t#{json_response}"
            raise "Did not receive a completed response within the timeout. Instead got:\n\t#{json_response}"
          end
        end

        # We want to narrow our success condition. So we test for succeeded
        # to indicate success. Anything else would indicate non-success.
        health_check = nil
        if json_response["executionState"] == "SUCCEEDED"
            health_check = HealthCheck.create(status: "healthy")
        else
            health_check = HealthCheck.create(status: "unhealthy")
        end

        return health_check
    end

    def self.run_health_check()
        # self.post_health_report
        follower = FollowOutput.new
        follower.follow_output(RundeckJob.last_health_report_id.to_s)
        services_status = {}
        service_name = nil
        while (follower.output_queue.size != 0)
            line = follower.output_queue.dequeue
            if line.starts_with?("changed:")
                service_name = line[line.index("(item=")+6..line.index(")")-1]
            end
            if line.starts_with?("STDOUT")
                puts "Found STDOUT"
                status = follower.output_queue.dequeue
                status = follower.output_queue.dequeue
                puts status
                services_status[service_name] = status
            end
        end
        if services_status == nil
            return nil
        end
        return services_status
    end

    def self.check_health()
        # Identical to Commands Receivable Check Health
        statuses = self.run_health_check()
        if statuses.blank?
            health_check = HealthCheck.create(status: "unhealthy")
            raise "No statuses available to check, check rundeck connection!"
        end
        good = []
        bad = []
        statuses.each do |key,value|
            if value.starts_with?("healthy")
                good.append(key)
            else
                bad.append(key)
            end
        end
        health_check = nil
        if bad.length > 0 
            puts "STATUS: Creating Health Check: Unhealthy"
            health_check = HealthCheck.create(status: "unhealthy")
        else
            puts "STATUS: Creating Health Check: Healthy"
            health_check = HealthCheck.create(status: "healthy")
        end
        return health_check
    end

    def self.post_compositional_role
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        @RundeckRoleBranch = RundeckKey.last_role_branch

        compositional_role_job_id = self.compositional_role_job_id
        project_url = "/rundeck/api/14/job/#{compositional_role_job_id}/run"
        base_url = self.base_server_url
        form_data = "-domain #{@RundeckCustomerBaseURL} -envvaultpass #{@RundeckVaultPassword} -branch #{@RundeckRoleBranch} -playbranch #{self.get_project_branch}"

        response = self.rundeck_post(base_url, project_url, form_data)
        json_response = JSON.parse(response)
        # Log an error status if there's an error reported in the result
        if json_response["error"] == true
            logger = Rails.logger
            logger.error "Got an 'error' in the JSON Response:\n#{json_response['error']}"
            raise "Got an 'error' in the JSON Response:\n#{json_response['error']}"
        end

        HealthCheck.create(status: "compositional_run")
        RundeckJob.create(job_id: self.compositional_role_job_id, job_name:"run_compositional_role", rundeck_job_id: json_response["id"])
        return json_response
    end


    def self.post_update_environment_repo
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        @RundeckRoleBranch = RundeckKey.last_role_branch

        update_environment_repo_job_id = self.update_environment_repo_job_id
        project_url = "/rundeck/api/14/job/#{update_environment_repo_job_id}/run"
        base_url = self.base_server_url
        form_data = "-domain #{@RundeckCustomerBaseURL} -envvaultpass #{@RundeckVaultPassword} -branch #{@RundeckRoleBranch} -playbranch #{self.get_project_branch}"

        response = self.rundeck_post(base_url, project_url, form_data)
        json_response = JSON.parse(response)
        # Log an error and raise an exception if there's an error reported in the result
        if json_response["error"] == true
            logger = Rails.logger
            logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
            raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
        end

        RundeckJob.create(job_id: self.update_environment_repo_job_id, job_name:"update_environment_repo", rundeck_job_id: json_response["id"])
        return json_response
    end

    def self.get_update_environment_repo(post_response)
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url

        # Set the variables that aren't going to change inside the loop
        project_url = "/rundeck/api/10/execution/#{post_response['id']}/state"
        base_url = self.base_server_url

        json_response = nil
        prevent_infinite_loop = 0
        while prevent_infinite_loop < 30
          # Get the result of the run. If this errors out, we're catching all exceptions in the
          # calling function that invoked this.
          response = self.rundeck_get(base_url, project_url)
          # Same with this, if we can't JSON.parse, then that exception can get caught in the
          # calling function that invoked this.
          json_response = JSON.parse(response)
          if json_response["error"] == true
              logger = Rails.logger
              logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
              raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
          end
          break if json_response["completed"] == true
          sleep(5)
          # This variable here makes sure that we don't wait in perpetuity
          # for a completed response just in case one doesn't actually come back.
          prevent_infinite_loop = prevent_infinite_loop + 1
          # Put in error statement if we hit the limit
          if prevent_infinite_loop == 30
            logger = Rails.logger
            logger.error "Did not receive a completed response within the timeout. Instead got:\n\t#{json_response}"
            raise "Did not receive a completed response within the timeout. Instead got:\n\t#{json_response}"
          end
        end

        # We want to narrow our success condition. So we test for succeeded
        # to indicate success. Anything else would indicate non-success.
        # json_response["executionState"] == "SUCCEEDED"
        return json_response
    end


    def self.full_backup
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        @RundeckRoleBranch = RundeckKey.last_role_branch

        backup_job_id = self.full_backup_job_id
        project_url = "/rundeck/api/14/job/#{backup_job_id}/run"
        base_url = self.base_server_url
        form_data = "-domain #{@RundeckCustomerBaseURL} -envvaultpass #{@RundeckVaultPassword} -branch #{@RundeckRoleBranch} -playbranch #{self.get_project_branch}"

        response = self.rundeck_post(base_url, project_url, form_data)
        json_response = JSON.parse(response)

        RundeckJob.create(job_id: self.migrate_instance_job_id, job_name:"backup", rundeck_job_id: json_response["id"])
    end

    private

    def self.rundeck_get(base_url, query_string)
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        uri = URI(base_url + query_string)

        # Get the URI so that we can use the headers going forward.
        req = Net::HTTP::Get.new(uri)
        req['X-Rundeck-Auth-Token'] = @RundeckAPIToken
        req['Accept'] = 'application/json'
        req['Content-Type'] = 'application/json'

        res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request(req)}

        return res.body
    end

    def self.rundeck_post(base_url, query_string, form_data)
        @RundeckAPIToken = RundeckKey.last_api_token
        @RundeckVaultPassword = RundeckKey.last_vault_password
        @RundeckCustomerBaseURL = RundeckKey.last_base_url
        uri = URI(base_url + query_string)

        req = Net::HTTP::Post.new(uri)
        req['X-Rundeck-Auth-Token'] = @RundeckAPIToken
        req['Accept'] = 'application/json'
        req.set_form_data("argString" => form_data)

        res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request(req)}

        return res.body
    end

    def self.base_server_url
        if ENV["RUNDECK_SERVER_URL"] != nil
            return ENV["RUNDECK_SERVER_URL"]
        else
            return Rails.application.credentials.rundeck[:server_url]
        end
    end

    def self.migrate_instance_job_id
        if ENV["MIGRATE_INSTANCE_JOB_ID"] != nil
            return ENV["MIGRATE_INSTANCE_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:migrate_instance_job_id]
        end
    end

    def self.health_report_job_id
        if ENV["HEALTH_REPORT_JOB_ID"] != nil
            return ENV["HEALTH_REPORT_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:health_report_job_id]
        end
    end

    def self.compositional_role_job_id
        if ENV["COMPOSITIONAL_ROLE_JOB_ID"] != nil
            return ENV["COMPOSITIONAL_ROLE_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:compositional_role_job_id]
        end
    end

    def self.full_backup_job_id
        if ENV["FULL_BACKUP_JOB_ID"] != nil
            return ENV["FULL_BACKUP_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:full_backup_job_id]
        end
    end

    def self.update_environment_repo_job_id
        if ENV["UPDATE_ENVIRONMENT_REPO_JOB_ID"] != nil
            return ENV["UPDATE_ENVIRONMENT_REPO_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:update_environment_repo_job_id]
        end
    end

    def self.restart_service_job_id
        if ENV["RESTART_SERVICE_JOB_ID"] != nil
            return ENV["RESTART_SERVICE_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:restart_service_job_id]
        end
    end

    def self.stop_service_job_id
        if ENV["STOP_SERVICE_JOB_ID"] != nil
            return ENV["STOP_SERVICE_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:stop_service_job_id]
        end
    end

    def self.run_ourcompose_role_job_id
        if ENV["RUN_OURCOMPOSE_ROLE_JOB_ID"] != nil
            return ENV["RUN_OURCOMPOSE_ROLE_JOB_ID"]
        else
            return Rails.application.credentials.rundeck[:run_ourcompose_role_job_id]
        end
    end

    class FollowOutput
        attr_accessor :output_queue
        class Queue
            attr_reader :items
          
            def initialize
                @items = []
            end
            
            def enqueue(item)
                @items.unshift(item)
            end
          
            def dequeue
                @items.pop
            end
          
            def size
                @items.length
            end
        end

        def initialize()
            self.output_queue = Queue.new
        end
    
        def follow_output(job_id)
            job_state = "/rundeck/api/10/execution/#{job_id}/state"
            execution_output_location = "/rundeck/api/5/execution/#{job_id}/output"
            
            status = nil
            timeout = 0
            currentLocation = 0
            while(timeout < 10)
                response = Rundeck.rundeck_get(Rundeck.base_server_url, job_state)
                json_response = JSON.parse(response)
                if json_response["error"] == true
                    logger = Rails.logger
                    logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
                    raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
                end
                status = json_response['executionState']
                status = status.downcase
                
                if status == "running"
                    timeout = 0
                elsif (status == "succeeded" || status == "failed" || status == "aborted")
                    timeout = 30
                else
                    timeout = 8
                end
                timeout = timeout + 1
                
                output_response = Rundeck.rundeck_get(Rundeck.base_server_url, execution_output_location)
                output_response_json = JSON.parse(output_response)
                if output_response_json["error"] == true
                    logger = Rails.logger
                    logger.error "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
                    raise "Got an 'error' in the JSON Response:\n\t#{json_response['message']}"
                end
                output = output_response_json['entries']

                while currentLocation < output.length
                    # Do stuff with each line of output
                    puts output[currentLocation]['log']
                    self.output_queue.enqueue(output[currentLocation]['log'])
                    currentLocation = currentLocation+1
                end
            end
        end
    end

end
