namespace :ansible do
  desc "Checks to see if the dirs exist"
  task :setup_env => :environment do
    unless setup_dirs
      Rails.logger.error "Could not set up dirs appropriately"
      fail "Could not set up dirs appropriately"
    end
  end

  task :update_env => :environment do
    unless update_environment_repo
      Rails.logger.error "Could not update repo"
      fail "Could not set up dirs appropriately"
    end
  end

  def setup_dirs
    Rails.logger.debug "Setting up dirs..."
    Rails.logger.debug "Rails Storage Root: #{Rails.application.config.storage.root}"
    storage_root = Rails.application.config.storage.root

    # Make sure the ansible directory exists
    unless Dir.exist?(storage_root + "/ansible")
      Rails.logger.debug "Could not find directory: #{Rails.application.config.storage.root}/ansible"
      Rails.logger.debug "Creating..."
      Dir.mkdir storage_root + "/ansible"
    end

    # Test for git
    unless system('which git')
      Rails.logger.error "Could not find 'git' executable in $PATH"
      return false
    end

    # Make sure the environment directory exists
    unless Dir.exist?(storage_root + "/ansible/environment")
      Rails.logger.debug "Could not find directory: #{Rails.application.config.storage.root}/ansible/environment"
      # Environment is a controlled git repo. We'll need a read-only user to access it
      # This means that we'll have to create an ourcomposebot_ro user and store creds
      # and all that good stuff. We'll also be linked to the gitlab repo for the time
      # being. I don't know if we want to hardcode different logic in here to see if
      # we can recreate it locally.
      #
      # Or at least we'd have to do something where there's a demo master environment
      # template available, since we wouldn't be deploying read-only users to our repo
      # with the keys available. Because if those keys change, then all existing
      # instances would be borked. I'd need like a public master branch, and all other
      # branches (including their existance) to be private. Either that, or just a
      # separate repo with the environment. That's what this comment seems to be pointing
      # to: https://forum.gitlab.com/t/private-branch-public-repo/3136
      unless ENV['OURCOMPOSEBOT_RO_KEY'].blank?
        #
        # Clone the private repo with the RO bot user's creds
        #
        # Make the app-specific ssh keys dir
        unless Dir.exist?(Rails.application.config.storage.tmp + "/.ssh")
          Dir.mkdir Rails.application.config.storage.tmp + '/.ssh'
        end

        # Write the ssh key
        #
        # When we're doing this, the env var should be looking like this:
        #
        # export OURCOMPOSEBOT_RO_KEY='-----BEGIN OPENSSH PRIVATE KEY-----
        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX==
        # -----END OPENSSH PRIVATE KEY-----
        # '
        #
        # And EXACTLY like that. With the line breaks for the new lines. The first
        # line being right there, and the ending quote being on the following line.
        # All of this serves to create the appropriate env var that gets put into
        # the file on the following lines
        File.open("#{Rails.application.config.storage.tmp}/.ssh/id_rsa", 'w+') do |f|
            f.write(ENV['OURCOMPOSEBOT_RO_KEY'])
            File.chmod(0600, "#{Rails.application.config.storage.tmp}/.ssh/id_rsa")
        end

        #
        # Clone the repo
        #
        # IdentitiesOnly=yes gets us the functionality that the identity of the SSH key is used
        # StrictHostKeyChecking=no bypasses the confirmation dialog for trusting unknown host keys
        git_ssh_cmd = "GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no -o IdentitiesOnly=yes -i #{Rails.application.config.storage.tmp}/.ssh/id_rsa'"
        # Here we get the base_url, but we use duplicate so that we can mutate the string later, so
        # it's a copy, not a borrow
        branch_name = ENV['BASE_URL'].dup
        # Replace all of the '.'s with '-'s in the domain to get the branch name that we need to clone
        branch_name.tr!('.', '-')
        # Invoke the actual git clone commandline command
        Rails.logger.debug "Cloning #{branch_name} branch of the environment repo..."
        system("#{git_ssh_cmd} git clone --branch #{branch_name} git@gitlab.com:compositionalenterprises/environment.git #{Rails.application.config.storage.root}/ansible/environment")
      else
        Rails.logger.debug "Cloning the master branch of the template repo..."
        # Clone the master branch of the template repo
        system("git clone https://gitlab.com/compositionalenterprises/template-environment.git #{Rails.application.config.storage.root}/ansible/environment")
        File.open("#{Rails.application.config.storage.root}/ansible/environment/group_vars/compositional/all.yml", 'a') do |line|
          line.puts "\r" + 'ourcompose_common_services:'
          ENV['SERVICES'].split.each do |service_name|
            line.puts "\r" + "  - #{service_name}"
          end
        end
      end
    end
  end

  def update_environment_repo
    Rails.logger.debug "Updating Repo..."
    Rails.logger.debug "Rails Storage Root: #{Rails.application.config.storage.root}"
    storage_root = Rails.application.config.storage.root

    # Check for an update to the repo only if we are on CE infrastructure
    unless ENV['OURCOMPOSEBOT_RO_KEY'].blank?
      Rails.logger.debug "Checking for updates..."
      # Make the app-specific ssh keys dir
      unless Dir.exist?(Rails.application.config.storage.tmp + "/.ssh")
        Dir.mkdir Rails.application.config.storage.tmp + '/.ssh'
      end
      # Write the ssh key
      #
      # When we're doing this, the env var should be looking like this:
      #
      # export OURCOMPOSEBOT_RO_KEY='-----BEGIN OPENSSH PRIVATE KEY-----
      # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX==
      # -----END OPENSSH PRIVATE KEY-----
      # '
      #
      # And EXACTLY like that. With the line breaks for the new lines. The first
      # line being right there, and the ending quote being on the following line.
      # All of this serves to create the appropriate env var that gets put into
      # the file on the following lines
      File.open("#{Rails.application.config.storage.tmp}/.ssh/id_rsa", 'w+') do |f|
          f.write(ENV['OURCOMPOSEBOT_RO_KEY'])
          File.chmod(0600, "#{Rails.application.config.storage.tmp}/.ssh/id_rsa")
      end
      git_ssh_cmd = "GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no -o IdentitiesOnly=yes -i #{Rails.application.config.storage.tmp}/.ssh/id_rsa'"

      #
      # Update the repo here
      #
      # What we have to be concerned about is not overwriting something that has been changed.
      # Of course, we don't have to worry about that if we do commits every time that something
      # gets changed and deal with the resolution of that in that section. However, as that has
      # yet to be written, we'll at least do some remedial sanity-checks here and saving of any
      # changes, just in case.
      directory = "#{storage_root}/ansible/environment"
      git_stash_command = "git --git-dir=#{directory}/.git --work-tree=#{directory} stash"
      git_pull_command = "git --git-dir=#{directory}/.git --work-tree=#{directory} pull"
      git_pop_command = "git --git-dir=#{directory}/.git --work-tree=#{directory} stash pop || echo 'No stash to pop'"
      system("#{git_ssh_cmd} #{git_stash_command}")
      system("#{git_ssh_cmd} #{git_pull_command}")
      system("#{git_ssh_cmd} #{git_pop_command}")
    end
  end
end
