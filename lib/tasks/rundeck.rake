namespace :rundeck do
    desc "Checks to see if the database exists"
    task :backup do
        begin
            Rake::Task['environment'].invoke
            ActiveRecord::Base.connection
        rescue
            puts "Database doesn't exist or isn't on!"
            AdminAlertJob.perform_now("#{ENV['BASE_URL']} backups failed", "Database doesn't exist or isn't on!")
            exit 1
        else
            Rundeck.full_backup
            if RundeckJob.last.rundeck_job_id == nil
                AdminAlertJob.perform_now("#{ENV['BASE_URL']} backups failed", "Exit 1 - Please Check Logs")
                puts "Exit 1 - Please Check Logs"
                exit 1
            end
            exit 0
        end
    end

    desc "Run Compositional Role"
    task :run_compositional_role do
        begin
            Rake::Task['environment'].invoke
            ActiveRecord::Base.connection
            Rundeck.post_compositional_role
            exit 0
        rescue
            exit 1
        end
    end
end

