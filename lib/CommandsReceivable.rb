module CommandsReceivable
    def self.run_health_check()
        socket_connection = UNIXSocket.new("/var/run/commands_receivable.sock")
        vault_pass = ENV['ENVIRONMENT_VAULT_PASSWORD'].blank? ? 'notnil' : ENV['ENVIRONMENT_VAULT_PASSWORD']
        pass_string = "{'script': 'playbooks/report_health.yml', 'vault_password': '#{vault_pass}', 'collection_version': '#{ENV['ROLE_BRANCH']}'}"
        socket_connection.print(pass_string)
        socket_connection.close_write
        services_status = {}
        begin
            service_name = nil
            while true
                line = socket_connection.readline()
                if line.to_s.starts_with?("changed:")
                    service_name = line[line.index("(item=")+6..line.index(")")-1]
                end
                if line.to_s.starts_with?("STDOUT")
                    puts "Found STDOUT"
                    status = socket_connection.readline.to_s.delete("\n")
                    puts status
                    services_status[service_name] = status
                end
            end
        rescue EOFError
            puts "End of File, do nothing"
        end
        socket_connection.close
        if services_status == nil
            return nil
        end
        return services_status
    rescue SocketError => e
        logger = Rails.logger
        logger.error "Ran into some kind of general socket error"
        return nil
    rescue Errno::ECONNREFUSED => e
        logger = Rails.logger
        logger.error "Ran into a Socket Connection Error"
        return nil
    end

    def self.check_health()
        statuses = self.run_health_check()
        if statuses.blank?
            health_check = HealthCheck.create(status: "unhealthy")
            raise "No statuses available to check, check socket!"
        end
        good = []
        bad = []
        statuses.each do |key,value|
            if value.starts_with?("healthy")
                good.append(key)
            else
                bad.append(key)
            end
        end
        health_check = nil
        if bad.length > 0 
            puts "STATUS: Creating Health Check: Unhealthy"
            health_check = HealthCheck.create(status: "unhealthy", service_status: statuses.to_s)
        else
            puts "STATUS: Creating Health Check: Healthy"
            health_check = HealthCheck.create(status: "healthy", service_status: statuses.to_s)
        end
        return health_check
    end
end
