# README

This repo is the static page for customer views. It is deployed with Ansible to the customer server and will serve as the home static page for customer instances.

This repo is meant for use with: 
  - Ruby: ruby 2.6.3p62 (2019-04-16 revision 67580) [x86_64-darwin18]
  - Rails: 6.0.0
  - Webpacker: 4.0.7
  - Node: v10.16.3
  - Yarn: 1.16.0
  - Docker: Docker version 19.03.1, build 74b1e89
  - Docker-compose: docker-compose version 1.24.1, build 4667896b


## Getting Started

I want this to be as smooth as possible, so start here!

Docker-compose should get you started, but please see the [docs folder](./docs/) for more info on building the project! The project can be found on dockerhub [here](https://hub.docker.com/repository/docker/compositionalenterprises/homstatic)!


## Setting Environment Variables

The environment needs to be set with the following environment variables (set them to what you need!):

```ini
DB_HOST=127.0.0.1
DB_NAME=skeleton
DB_USER=user
DB_PASS=password

BASE_URL=user1.compositional.enterprises
ORG_NAME=orgname
ADMIN_EMAIL=example@example.com
ADMIN_PASSWORD=P4$$word123
SERVICES="kanboard wordpress bitwarden"

```

## Docker Volumes

Setting the production credentials using docker volumes sets secret variables within the application. By default the application uses `sample` credentials. The production credentials are required as they pass the mailgun credentials.
```sh
docker run -p 3000:3000 \ 
  -v /path/to/production.key:/app/config/credentials/production.key \
  -v /path/to/production.yml.enc:/app/config/credentials/production.yml.enc
  compositionalenterprises/homstatic
```

## Seeding the Database

The application will need to be seeded with some information, particularly the initial admin email and the services:

```sh
# Seed Customizations
rake db:seed:single SEED=org_setup ORG_NAME="example" BASE_URL="example.com"

# Seed Admin
rake db:seed:single SEED=admin EMAIL="example@example.com" PASSWORD="P4$$WORD"

# Seed Services
rake db:seed:single SEED=services SERVICES="kanboard bitwarden" BASE_URL="example.com"
```

These seeds can be set with environment variables, but the seeds can be run independently of the docker entrypoint script.

## License
MIT

## Attribution

[Background CSS](https://www.svgbackgrounds.com/)

## Author Info

Don't hesitate to reach out: Jack Moore, [support@ourcompose.com](mailto:support@ourcompose.com)

