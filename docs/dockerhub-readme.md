# Homstatic

The Homstatic application is a rails application that acts as a landing page for an instance of a compositional deploy. The repo can be found on [gitlab here](https://gitlab.com/compositional-enterprises/homstatic)

## Getting Started

Make sure to have a mariadb or mysql database available for this application to work with.

Pull down latest with:
```sh
docker pull compositionalenterprises/homstatic:latest
```

An example of running a container of the application for a barebones deploy without email configured looks like the following:

```sh
docker run -p 3000:3000 \ 
  -v ~/production.key:/app/config/credentials/production.key \
  -v ~/production.yml.enc:/app/config/credentials/production.yml.enc \
  -e DB_HOST=host.docker.internal \
  -e DB_NAME=skeleton \
  -e DB_USER=user \
  -e DB_PASS=password \
  compositionalenterprises/homstatic
```

### Environment Variables

  - DB_HOST: 'database'
  - DB_DATABASE: 'portal'
  - DB_USERNAME: 'portal'
  - DB_PASSWORD: "{{ compositional_portal_backend_password }}"
  - BASE_URL: "{{ environment_domain }}"
  - ORG_NAME: "{{ environment_domain.split('.')[0].upper() }}"
  - ADMIN_EMAIL: "{{ environment_admin }}@{{ environment_domain }}"
  - ADMIN_PASSWORD: "{{ compositional_portal_admin_password }}"
  - SERVICES: "{{ compositional_services | join(' ') }}"

After the deploy is complete; database seeding will need to run to seed the admin table, the services table, and the customization table.


## More Information

Please reach out to me for any questions, and for more information on the project please see the [readme on gitlab](https://gitlab.com/compositional-enterprises/homstatic/blob/master/README.md).
