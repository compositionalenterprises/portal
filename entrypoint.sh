#!/bin/sh

export RAILS_ENV=production

if [ ! -f ./config/credentials/production.key ]; then
    mv ./config/credentials/production.yml.enc ./config/credentials/production.yml.bak
    mv ./config/credentials/sample.key ./config/credentials/production.key
    mv ./config/credentials/sample.yml.enc ./config/credentials/production.yml.enc
fi

bundle exec rake db:exists && rake db:migrate || rake db:setup

## Seeds
bundle exec rake db:seed:single SEED=org_setup ORG_NAME="${ORG_NAME}" BASE_URL="${BASE_URL}"
bundle exec rake db:seed:single SEED=services SERVICES="${SERVICES}" BASE_URL="${BASE_URL}"
bundle exec rake db:seed:single SEED=keys RUNDECK_API_TOKEN="${RUNDECK_API_TOKEN}" ENVIRONMENT_VAULT_PASSWORD="${ENVIRONMENT_VAULT_PASSWORD}" BASE_URL="${BASE_URL}" ROLE_BRANCH="${ROLE_BRANCH}"
bundle exec rake ansible:setup_env

exec "$@"
