#!/bin/sh
# rake_admin.sh script
# ENVIRONMENT VARIBLES THAT NEED TO BE SET:
#   PORTAL_ADMIN_CREATE_UPDATE= (string) "CREATE" or "UPDATE"
#   PORTAL_ADMIN_EMAIL= (string) admin email address
#   PORTAL_ADMIN_PASSWORD= (string) admin password
#   PORTAL_ADMIN_SEND_EMAIL_FLAG= (string) "YES" or "NO"


export RAILS_ENV=production
bundle exec rake db:seed:single SEED=admin CREATE_UPDATE=$PORTAL_ADMIN_CREATE_UPDATE EMAIL=$PORTAL_ADMIN_EMAIL PASSWORD=$PORTAL_ADMIN_PASSWORD SEND_EMAIL_FLAG=$PORTAL_ADMIN_SEND_EMAIL_FLAG

