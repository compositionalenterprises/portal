

ENV['SERVICES'].split.each do |service_name|
    if Service.find_by(name: service_name).nil?
        Service.create(name: service_name, url: ENV['BASE_URL'] + "/" + service_name, enabled: true)
    end
end

