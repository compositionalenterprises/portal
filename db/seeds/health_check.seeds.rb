#
# Every 10 minutes make the call out to "Run the Health Report" via Rundeck.post_health_report
# After the first call, make a check on the last call to "Run the health report" via Rundeck.get_health_report
# Based on the last status if the last report(s) come back "unhealthy", then reach out to admins
#
logger = Rails.logger
# Catch any errors, because why not.
begin
    ExecutionAbstractionHelper.run_health_report
rescue Exception => e
  # TODO: We should do something with 'e' here. Some kinda logging or something
  # so we actually see the error. Otherwise, we'll just kinda be at the whim of
  # Jack to actually figure out what went wrong. Can we log it to stderr or something
  # so the container logs would at least show the error?
  logger.error "Could not post health report.\nExited with the following error:\n\t#{e}"
  # Either way, let's alert here
  AdminAlertJob.perform_now("#{RundeckKey.last_base_url} is unknown", "Could not post health report. Exited with the following error:\n\n#{e}")
  abort "Could not post health report.\nExited with the following error:\n\t#{e}"
end


def check_run_alerts()
    last_status = HealthCheck.last(2).reverse[0].status
    second_last_status = HealthCheck.last(2).reverse[1].status

    #
    # First, we want to check if the latest result is _still_ "unhealthy" directly after a
    # compositional run. If that is the case, then the previous compositional run failed to
    # fix the problem, and we'll want to report this to the admins.
    #
    if last_status == 'unhealthy' && second_last_status == 'compositional_run'
        AdminAlertJob.perform_now("#{RundeckKey.last_base_url} having problems", "Failed auto-fix. Please investigate.")
    #
    # Next, we want to catch the generic 'something is wrong, let's try to fix it'. But, if we have two 'unhealthy' statuses
    # in a row, it's likely that we've tried to fix, and failed, and all subsequent runs will return unhealthy. Since we've
    # presumably already sent out the "Please Investigate" message (above), then we just skip whenever we have two unhealthy
    # statuses in a row. We only want an unheathy status that was preceeded by a 'healthy' status.
    #
    elsif last_status == 'unhealthy' && second_last_status != 'unhealthy'
        AdminAlertJob.perform_now("#{RundeckKey.last_base_url} having problems", "Trying to auto-fix...")
        # Only return True here because the very first time the failure occurs, we want to fix it,
        # otherwise we are either healthy, or the previous attempt to fix has failed, so we aren't going to
        # try it again.
        return true
    elsif last_status == 'healthy' && second_last_status == 'compositional_run'
        # Yay!!! We fixed it!!!
        AdminAlertJob.perform_now("#{RundeckKey.last_base_url} is healthy", "Instance Self-healed")
    end

    #
    # We return false as default because there should be no fix in almost all situations except for the
    # return True above.
    #
    return false
end


# Catch any errors, because why not.
begin
    if RundeckJob.last_health_report_id.nil?
        logger.debug "\tLast Health Report query returned #{RundeckJob.last_health_report_id}\n\tSkipping Health Report Results Retrieval"
    end
    # We're ensuring that there are indeed at least two there
    # because we need to parse both of them
    if HealthCheck.last(2).reverse[1].nil?
        logger.debug "\tNot checking whether we need a fix or not; not enough health_checks in the database. Currently there are #{HealthCheck.count}"
    else
        logger.debug "\tChecking on whether we need to run a fix."
        # Figure out if it's appropriate to try to run the fix
        run_fix = check_run_alerts()
        if run_fix
            logger.debug "\tDetermined a need to run a fix."
            # Actually try to fix the instance
            ExecutionAbstractionHelper.run_compositional_role
        else
            logger.debug "\tDid not Determine a need to run a fix."
        end
        return 0
    end
rescue Exception => e
  # TODO: We should do something with 'e' here. Some kinda logging or something
  # so we actually see the error. Otherwise, we'll just kinda be at the whim of
  # Jack to actually figure out what went wrong. Can we log it to stderr or something
  # so the container logs would at least show the error?
  logger.error "Could not get health report and/or run compositional role.\nExited with the following error:\n\t#{e}"
  # Either way, let's alert here
  AdminAlertJob.perform_now("#{RundeckKey.last_base_url} is unknown", "Could not get health report and/or run compositional run. Exited with the following error:\n\n#{e}")
  abort "Could not perform health report and/or run compositional role.\nExited with the following error:\n\t#{e}"
end
