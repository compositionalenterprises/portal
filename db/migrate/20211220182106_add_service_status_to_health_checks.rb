class AddServiceStatusToHealthChecks < ActiveRecord::Migration[6.1]
  def change
    change_table :health_checks do |t|
      t.text :service_status
    end
  end
end
