class CreateAdminAlerts < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_alerts do |t|
      t.string :message
      t.string :subject
      t.integer :count

      t.timestamps
    end
  end
end
