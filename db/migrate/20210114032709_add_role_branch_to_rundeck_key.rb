class AddRoleBranchToRundeckKey < ActiveRecord::Migration[6.0]
  def change
    change_table :rundeck_keys do |t|
      t.string :role_branch
    end
  end
end
