class CreateRundeckKeys < ActiveRecord::Migration[6.0]
  def change
    create_table :rundeck_keys do |t|
      t.string :rundeck_api_token
      t.string :vault_password
      
      t.timestamps
    end
  end
end
