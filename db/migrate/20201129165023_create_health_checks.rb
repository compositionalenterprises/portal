class CreateHealthChecks < ActiveRecord::Migration[6.0]
  def change
    create_table :health_checks do |t|
      t.string :status #healthy, unhealthy, compositional_run
      t.datetime :last_run_compositional_role
      t.datetime :last_status_request
      t.references :rundeck_job
      t.timestamps
    end
  end
end
