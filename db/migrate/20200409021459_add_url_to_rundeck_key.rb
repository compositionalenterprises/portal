class AddUrlToRundeckKey < ActiveRecord::Migration[6.0]
  def change
    change_table :rundeck_keys do |t|
      t.string :base_url
    end
  end
end
