class CreateCustomizations < ActiveRecord::Migration[6.0]
  def change
    create_table :customizations do |t|
      t.string :base_url
      t.string :tagline
      t.string :org_name
      t.string :org_name_url
      t.timestamps
    end
  end
end
