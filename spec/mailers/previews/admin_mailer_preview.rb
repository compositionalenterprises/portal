# Preview all emails at http://localhost:3000/rails/mailers/admin_mailer
class AdminMailerPreview < ActionMailer::Preview
    def confirmation_instructions
        AdminMailer.confirmation_instructions(Admin.first, "FakeToken", {password: "fake-password"})
    end
end
