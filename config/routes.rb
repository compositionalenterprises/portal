Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions', 
    registrations: 'users/registrations', 
    confirmations: 'users/confirmations'
  }

  devise_for :admins, skip: [:registrations]
  devise_scope :admin do
    unauthenticated :admin do
      root 'devise/sessions#new'
    end
    authenticated :admin do
      devise_for :admins, only: [:registrations], controllers: { registrations: 'admins/registrations' }
      root to: "customize#show", as: :authenticated_user
      resources :admins, only: [:index, :show]
      resource :customize, controller: 'customize', except: [:edit, :new] do
        get 'data', to: 'data'
      end
      resource :compositional_role_variable, as: 'env', path: 'env' do
        get 'env_vars', to: 'get_env_vars'
        post 'update', to: 'update', as: 'update_env'
      end 
      resource :services, controller:'services', except: [:edit, :new]
      namespace :rundeck do
        post 'migrate', to: 'migrate'
        get 'migration_status', to: 'migration_status'
        post 'run_compositional_role', to: 'run_compositional_role'
      end
      namespace :execution_abstraction do
        # Post Methods for Running Commands
        post 'migrate', to: 'post_migrate', action:'post_migrate'
        post 'compositional_role', to: 'post_compositional_role', action: 'post_compositional_role'
        post 'health_check', to: 'post_health_check', action: 'post_health_check'
        post 'run_backup', to: 'post_run_backup', action: 'post_run_backup'
        post 'set_rundeck_key', to: 'set_rundeck_key', action: 'set_rundeck_key'
        post 'service_state', to: 'post_service_state', action: 'post_service_state'
        post 'run_ourcompose_role', to: 'post_run_ourcompose_role', action: 'post_run_ourcompose_role'
        post 'env_configuration', to: 'post_env_configuration', action: 'post_env_configuration'

        # Get Requests for latest
        get 'migration', to: 'get_migration', action: 'get_migration'
        get 'compositional_role', to: 'get_compositional_role', action: 'get_compositional_role'
        get 'health_check', to: 'get_health_check', action: 'get_health_check'
        get 'backup', to: 'get_backup', action: 'get_backup'
        get 'service_state', to: 'get_service_state', action: 'get_service_state'
        get 'env_configuration', to: 'get_env_configuration', action: 'get_env_configuration'

      end
      resource :proxy_logs, as: 'logs', path: 'logs', only: [:show] do
        get 'available', to: 'get_available_log_files', action: 'available_log_files'
        get 'get_file', to: 'get_log_file', action: 'log_file'
        get 'logfile_length', to: 'logfile_length', action: 'logfile_length'
      end
    end
  end

  get 'help', to: 'staticpages#help'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #root 'staticpages#index'

end
